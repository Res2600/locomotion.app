<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->boolean("show_owner_as_contact")->default("true");
        });
    }

    public function down()
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->dropColumn("show_owner_as_contact");
        });
    }
};
