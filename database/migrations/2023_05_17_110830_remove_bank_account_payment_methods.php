<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        DB::table("payment_methods")
            ->where("type", "bank_account")
            ->delete();

        Schema::table("payment_methods", function (Blueprint $table) {
            $table->dropColumn("type");
        });
    }

    public function down(): void
    {
        Schema::table("payment_methods", function (Blueprint $table) {
            $table->enum("type", ["credit_card", "bank_account"]);
        });
    }
};
