<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::drop("taggables");
        Schema::drop("tags");
    }

    public function down(): void
    {
        Schema::create("tags", function (Blueprint $table) {
            $table->bigIncrements("id");

            $table->string("name");
            $table->enum("type", ["tag"])->default("tag");
            $table->string("slug");

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create("taggables", function (Blueprint $table) {
            $table->bigIncrements("id");

            $table->string("taggable_type")->nullable();
            $table
                ->integer("taggable_id")
                ->unsigned()
                ->nullable();

            $table->unsignedBigInteger("tag_id");

            $table
                ->foreign("tag_id")
                ->references("id")
                ->on("tags")
                ->onDelete("cascade");

            $table->index(["taggable_id", "taggable_type"]);
            $table->index("tag_id");
        });
    }
};
