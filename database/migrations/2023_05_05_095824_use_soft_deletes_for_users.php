<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        User::where("is_deactivated", true)
            ->whereNull("deleted_at")
            ->update(["deleted_at" => DB::raw("updated_at")]);

        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn("suspended_at");
            $table->dropColumn("is_deactivated");
        });
    }

    public function down(): void
    {
        Schema::table("users", function (Blueprint $table) {
            $table->dateTimeTz("suspended_at")->nullable();
            $table->boolean("is_deactivated")->default(0);
        });

        User::whereNotNull("deleted_at")->update(["is_deactivated" => true]);
    }
};
