<?php
namespace Database\Factories;
use App\Models\Takeover;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TakeoverFactory extends Factory
{
    protected $model = Takeover::class;
    public function definition(): array
    {
        return [
            "executed_at" => Carbon::now(),
            "status" => $this->faker->randomElement([
                "in_process",
                "canceled",
                "completed",
            ]),
            "mileage_beginning" => $this->faker->numberBetween(0, 300_000),
            "comments_on_vehicle" => $this->faker->sentence,
            "contested_at" => null,
            "comments_on_contestation" => null,
        ];
    }

    public function completed()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "completed",
        ]);
    }

    public function inProcess()
    {
        return $this->state([
            "executed_at" => null,
            "status" => "in_process",
            "mileage_beginning" => null,
            "comments_on_vehicle" => null,
        ]);
    }

    public function canceled()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "canceled",
        ]);
    }
}
