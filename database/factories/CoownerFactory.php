<?php

namespace Database\Factories;

use App\Models\Coowner;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CoownerFactory extends Factory
{
    protected $model = Coowner::class;

    public function definition(): array
    {
        return [
            "show_as_contact" => $this->faker->boolean(),
            "title" => $this->faker->word(),
            "user_id" => User::factory(),
        ];
    }
}
