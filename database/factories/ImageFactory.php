<?php
namespace Database\Factories;
use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    protected $model = Image::class;
    public function definition(): array
    {
        return [
            "original_filename" => $this->faker->word,
            "path" => $this->faker->imageUrl(),
            "filename" => $this->faker->word,
            "width" => $this->faker->numberBetween(100, 500),
            "height" => $this->faker->numberBetween(100, 500),
            "filesize" => $this->faker->numberBetween(1000, 10_000),
        ];
    }
}
