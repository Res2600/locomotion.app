<?php
namespace Database\Factories;
use App\Models\PrePayment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrePaymentFactory extends Factory
{
    protected $model = PrePayment::class;

    public function definition(): array
    {
        return [
            "executed_at" => Carbon::now(),
            "status" => $this->faker->randomElement([
                "in_process",
                "canceled",
                "completed",
            ]),
        ];
    }

    public function completed()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "completed",
        ]);
    }

    public function inProcess()
    {
        return $this->state([
            "executed_at" => null,
            "status" => "in_process",
        ]);
    }

    public function canceled()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "canceled",
        ]);
    }
}
