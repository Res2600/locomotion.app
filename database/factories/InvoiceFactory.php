<?php
namespace Database\Factories;
use App\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    protected $model = Invoice::class;

    public function definition(): array
    {
        return [
            "period" => Carbon::now()
                ->locale("fr_FR")
                ->isoFormat("LLLL"),
            "paid_at" => Carbon::now(),
        ];
    }
}
