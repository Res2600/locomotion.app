<?php
namespace Database\Factories;
use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileFactory extends Factory
{
    protected $model = File::class;
    public function definition(): array
    {
        $filename = $this->faker->word . "." . $this->faker->fileExtension;
        return [
            "path" => $this->faker->word,
            "filename" => $filename,
            "original_filename" => $filename,
            "filesize" => $this->faker->randomNumber(),
        ];
    }
}
