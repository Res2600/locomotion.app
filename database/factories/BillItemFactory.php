<?php
namespace Database\Factories;
use App\Models\BillItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class BillItemFactory extends Factory
{
    protected $model = BillItem::class;
    public function definition(): array
    {
        return [
            "label" => $this->faker->word,
            "amount" => $this->faker->numberBetween(0, 300_000),
            "item_date" => $this->faker->date(),
        ];
    }
}
