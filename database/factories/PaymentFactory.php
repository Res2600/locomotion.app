<?php
namespace Database\Factories;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    protected $model = Payment::class;

    public function definition(): array
    {
        return [
            "executed_at" => Carbon::now(),
            "status" => $this->faker->randomElement([
                "in_process",
                "completed",
            ]),
        ];
    }

    public function completed()
    {
        return $this->state([
            // Complete it in the future, to avoid changing the loan duration
            "executed_at" => Carbon::now()->addYears(100),
            "status" => "completed",
        ]);
    }

    public function inProcess()
    {
        return $this->state([
            "executed_at" => null,
            "status" => "in_process",
        ]);
    }
}
