<?php

namespace Database\Seeders;
use App\Models\Loanable;
use App\Models\Trailer;
use Illuminate\Database\Seeder;

class TrailersTableSeeder extends Seeder
{
    public function run()
    {
        // Start trailers at 2001
        $trailers = [
            [
                "id" => 2001,
                "type" => "trailer",
                "name" => "Remorque Solon sans communauté",
                "position" => "45.54471 -73.628796",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "always",
                // solonpetitepatrie@locomotion.app
                "owner_id" => 3,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => true,
                "details" => [
                    "id" => 2001,
                    "maximum_charge" => "5kg",
                    "dimensions" => "10' x 4'",
                ],
            ],
            [
                "id" => 2002,
                "type" => "trailer",
                "name" => "Remorque Solon Ahuntsic",
                "position" => "45.563652 -73.654695",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "always",
                // solonahuntsic@locomotion.app
                "owner_id" => 2,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => true,
                "details" => [
                    "id" => 2002,
                    "maximum_charge" => "5kg",
                    "dimensions" => "10' x 4'",
                ],
            ],
            [
                "id" => 2003,
                "type" => "trailer",
                "name" => "Remorque Solon Petite-Patrie",
                "position" => "45.540 -73.610",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "always",
                // solonpetitepatrie@locomotion.app
                "owner_id" => 3,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => true,
                "details" => [
                    "id" => 2003,
                    "maximum_charge" => "5kg",
                    "dimensions" => "10' x 4'",
                ],
            ],
            [
                "id" => 2101,
                "type" => "trailer",
                "name" => "Remorque privée sur demande",
                "position" => "45.535 -73.605",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
  {
    "available":true,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                // proprietairepetitepatrie@locomotion.app
                "owner_id" => 6,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => false,
                "details" => [
                    "id" => 2101,
                    "maximum_charge" => "5kg",
                    "dimensions" => "10' x 4'",
                ],
            ],
            [
                "id" => 2102,
                "type" => "trailer",
                "name" => "Remorque privée en libre service",
                "position" => "45.540 -73.605",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
  {
    "available":true,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                // proprietairepetitepatrie@locomotion.app
                "owner_id" => 6,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => true,
                "details" => [
                    "id" => 2102,
                    "maximum_charge" => "5kg",
                    "dimensions" => "10' x 4'",
                ],
            ],
        ];

        foreach ($trailers as $loanable) {
            $trailer = $loanable["details"];
            unset($loanable["details"]);
            Loanable::create($loanable);
            Trailer::create($trailer);
        }

        \DB::statement(
            "SELECT setval('loanables_id_seq'::regclass, (SELECT MAX(id) FROM loanables) + 1)"
        );
    }
}
