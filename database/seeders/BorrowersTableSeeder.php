<?php

namespace Database\Seeders;
use App\Models\Borrower;
use DateTime;
use Illuminate\Database\Seeder;

class BorrowersTableSeeder extends Seeder
{
    public function run()
    {
        $borrowers = [
            [
                "id" => 1,
                "user_id" => 1,
            ],
            [
                "id" => 2,
                "user_id" => 2,
            ],
            [
                "id" => 3,
                "user_id" => 3,
            ],
            [
                "id" => 4,
                "user_id" => 4,
            ],
            [
                // emprunteurahuntsic@locomotion.app
                "id" => 5,
                "user_id" => 5,
                "drivers_license_number" => "L1234-456789-01",
                "has_not_been_sued_last_ten_years" => true,
                "submitted_at" => new DateTime(),
                "approved_at" => new DateTime(),
            ],
            [
                "id" => 6,
                "user_id" => 6,
            ],
            [
                // emprunteurpetitepatrie@locomotion.app
                "id" => 7,
                "user_id" => 7,
                "drivers_license_number" => "L1234-456789-01",
                "has_not_been_sued_last_ten_years" => true,
                "submitted_at" => new DateTime(),
                "approved_at" => new DateTime(),
            ],
            [
                "id" => 8,
                "user_id" => 8,
            ],
            [
                "id" => 9,
                "user_id" => 9,
            ],
        ];

        foreach ($borrowers as $borrower) {
            if (!Borrower::where("id", $borrower["id"])->exists()) {
                Borrower::withoutEvents(function () use ($borrower) {
                    Borrower::create($borrower);
                });
            } else {
                Borrower::where("id", $borrower["id"])->update($borrower);
            }
        }

        \DB::statement(
            "SELECT setval('borrowers_id_seq'::regclass, (SELECT MAX(id) FROM borrowers) + 1)"
        );
    }
}
