<?php

namespace Database\Seeders;
use App\Models\Car;
use App\Models\Loanable;
use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    public function run()
    {
        // Start cars at 1001
        $cars = [
            [
                "id" => 1001,
                "type" => "car",
                "name" => "Auto de Propriétaire Petite-Patrie sur demande",
                "position" => "45.540, -73.590",
                "location_description" => "Stationnée devant la maison.",
                "comments" => "",
                "instructions" => "",
                // proprietairepetitepatrie@locomotion.app
                "owner_id" => 6,
                "availability_json" => <<<JSON
[
  {
    "available":true,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                "created_at" => new \DateTime(),
                "availability_mode" => "never",

                "details" => [
                    "id" => 1001,
                    "brand" => "Toyota",
                    "model" => "Matrix",
                    "year_of_circulation" => "2015",
                    "transmission_mode" => "automatic",
                    "engine" => "fuel",
                    "plate_number" => "F123456",
                    "value_category" => "lte50k",
                    "papers_location" => "in_the_car",
                    "insurer" => "Assurancetourix",
                    "has_informed_insurer" => true,
                    "pricing_category" => "large",
                ],
            ],
        ];

        foreach ($cars as $loanable) {
            $car = $loanable["details"];
            unset($loanable["details"]);
            Loanable::create($loanable);
            Car::create($car);
        }

        \DB::statement(
            "SELECT setval('loanables_id_seq'::regclass, (SELECT MAX(id) FROM loanables) + 1)"
        );
    }
}
