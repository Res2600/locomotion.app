<?php

namespace Database\Seeders;
use App\Models\Community;
use App\Models\Pricing;
use Illuminate\Database\Seeder;

class ProductionPricingsTableSeeder extends Seeder
{
    public function run()
    {
        $pricings = [
            [
                "name" => "Emprunt gratuit",
                "object_type" => "trailer",
                "rule" => "0",
            ],
            [
                "name" => "Emprunt gratuit",
                "object_type" => "bike",
                "rule" => "0",
            ],
            [
                "name" => "Tarif Voitures LocoMotion",
                "object_type" => "car",
                "rule" => <<<RULE
# Attention :
# - Les taux sont répétés plus d'une fois dans la tarification. S'assurer de capter tous les cas.
# - Le chiffre 4 permet l'application d'un taux horaire égal au quart du taux par jour. Si on veut modifier cette progression pour qu'elle soit plutôt au sixième du taux par jour, par exemple, il faut remplacer les deux occurrences dans chaque expression.

SI \$OBJET.pricing_category == 'large' ALORS { time: MAX(\$KM * 0.27 + PLANCHER(\$MINUTES / (60*24)) * 22.77 + MIN(((\$MINUTES - (PLANCHER(\$MINUTES / (60*24)) * (60*24))) / 60), 4) * (22.77/4), 15), insurance: \$EMPRUNT.days * (\$PRIME_PAR_JOUR + \$SURCOUT_ASSURANCE) }

SI \$OBJET.pricing_category == 'small' ALORS { time: MAX(\$KM * 0.19 + PLANCHER(\$MINUTES / (60*24)) * 15.62 + MIN(((\$MINUTES - (PLANCHER(\$MINUTES / (60*24)) * (60*24))) / 60), 4) * (15.62/4), 15), insurance: \$EMPRUNT.days * (\$PRIME_PAR_JOUR + \$SURCOUT_ASSURANCE) }

SI \$OBJET.pricing_category == 'electric' ALORS { time: MAX(\$KM * 0.08 + PLANCHER(\$MINUTES / (60*24)) * 14.66 + MIN(((\$MINUTES - (PLANCHER(\$MINUTES / (60*24)) * (60*24))) / 60), 4) * (14.66/4), 15), insurance: \$EMPRUNT.days * (\$PRIME_PAR_JOUR + \$SURCOUT_ASSURANCE) }

# Tous les autres cas, mais il n'y en a pas d'autres normalement.
{ time: MAX(\$KM * 0.19 + PLANCHER(\$MINUTES / (60*24)) * 15.62 + MIN(((\$MINUTES - (PLANCHER(\$MINUTES / (60*24)) * (60*24))) / 60), 4) * (15.62/4), 15), insurance: \$EMPRUNT.days * (\$PRIME_PAR_JOUR + \$SURCOUT_ASSURANCE) }
RULE
            ,
            ],
        ];

        foreach (
            Community::where("type", "=", "borough")->get()
            as $community
        ) {
            foreach ($pricings as $data) {
                $pricing = new Pricing();
                $pricing->fill($data);
                $pricing->community()->associate($community);
                $pricing->save();
            }
        }

        \DB::statement(
            "SELECT setval('pricings_id_seq'::regclass, (SELECT MAX(id) FROM pricings) + 1)"
        );
    }
}
