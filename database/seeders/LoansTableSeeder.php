<?php

namespace Database\Seeders;

use App\Http\Controllers\LoanController;
use App\Models\Loan;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LoansTableSeeder extends Seeder
{
    public function run()
    {
        $loans = [
            [
                "id" => 1,
                "departure_at" => now(),
                "duration_in_minutes" => 120,
                "borrower_id" => 7,
                "loanable_id" => 3,
                "estimated_distance" => 20,
                "platform_tip" => 2,
                "reason" => "Promenade",
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
            ],
        ];

        foreach ($loans as $loan) {
            if (!Loan::where("id", $loan["id"])->exists()) {
                LoanController::loanActionsForward(Loan::create($loan));
            } else {
                Loan::where("id", $loan["id"])
                    ->first()
                    ->update($loan);
            }
        }

        Loan::factory()
            ->withInProcessIntention()
            ->create([
                "id" => 2,
                "borrower_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "departure_at" => Carbon::now()->subHour(),
            ]);
        Loan::factory()
            ->withInProcessPrePayment()
            ->create([
                "id" => 3,
                "borrower_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "departure_at" => Carbon::now()->subHours(2),
            ]);
        Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "id" => 4,
                "borrower_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "departure_at" => Carbon::now()->subHours(3),
            ]);
        Loan::factory()
            ->withInProcessHandover()
            ->create([
                "id" => 5,
                "borrower_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "departure_at" => Carbon::now()->subHours(4),
            ]);
        Loan::factory()
            ->withInProcessPayment()
            ->create([
                "id" => 6,
                "borrower_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "departure_at" => Carbon::now()->subHours(5),
            ]);

        \DB::statement(
            "SELECT setval('loans_id_seq'::regclass, (SELECT MAX(id) FROM loans) + 1)"
        );
    }
}
