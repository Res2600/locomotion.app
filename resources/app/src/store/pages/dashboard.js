import Vue from "vue";

const initialState = {
  loans: {
    future: {
      loans: [],
      total: 0,
    },
    started: {
      loans: [],
      total: 0,
    },
    contested: {
      loans: [],
      total: 0,
    },
    waiting: {
      loans: [],
      total: 0,
    },
    completed: {
      loans: [],
      total: 0,
    },
    need_approval: {
      loans: [],
      total: 0,
    },
  },
  loanables: {
    owned: {
      loanables: [],
      total: 0,
    },
    coowned: {
      loanables: [],
      total: 0,
    },
  },
  loaded: false,
  loansLoaded: false,
  loanablesLoaded: false,
  hasMoreLoanables: false,
  loadRequests: 0,
};

const maxLoanableCount = 5;

const actions = {
  async reload({ commit, dispatch }) {
    commit("reloading");
    dispatch("loadLoans");
    dispatch("loadLoanables");
  },
  async loadLoans({ commit }) {
    commit("loadLoans");

    try {
      const { data: loans } = await Vue.axios.get("/loans/dashboard");
      commit("loansLoaded", loans);
    } catch (e) {
      commit("errorLoading", e);
      throw e;
    }
  },
  async loadLoanables({ commit }) {
    commit("loadLoanables");

    try {
      const { data: loanables } = await Vue.axios.get("/loanables/dashboard");
      commit("loanablesLoaded", loanables);
    } catch (e) {
      commit("errorLoading", e);
      throw e;
    }
  },
};

const mutations = {
  reloading(state) {
    state.loansLoaded = false;
    state.loanablesLoaded = false;
  },
  loadLoans(state) {
    state.loadRequests++;
  },
  loadLoanables(state) {
    state.loadRequests++;
  },
  loansLoaded(state, loans) {
    state.loans = loans;
    state.loansLoaded = true;
    state.loadRequests--;
  },
  loanablesLoaded(state, loanables) {
    state.loanablesLoaded = true;
    state.loanables = loanables;
    state.hasMoreLoanables = loanables.total > maxLoanableCount;
    state.loadRequests--;
  },
  errorLoading(state) {
    state.loadRequests--;
  },
  setLoanables(state, loanables) {
    state.loanables = loanables;
  },
};

export default {
  namespaced: true,
  state: initialState,
  actions,
  mutations,
};
