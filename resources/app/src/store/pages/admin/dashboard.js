import Vue from "vue";

export default {
  namespaced: true,
  state: {
    dashboard: {},
    loading: false,
    loaded: false,
  },
  mutations: {
    loading(state) {
      state.loading = true;
      state.loaded = false;
    },
    doneLoading(state, dashboard) {
      state.loading = false;
      state.loaded = true;
      state.dashboard = dashboard;
    },
    errorLoading(state, error) {
      state.loading = false;
      state.loaded = false;
    },
  },
  actions: {
    async retrieve({ commit }) {
      commit("loading");

      try {
        const { data: dashboard } = await Vue.axios.get("/admin/dashboard");
        commit("doneLoading", dashboard);
      } catch (e) {
        commit("errorLoading", e);
        throw e;
      }
    },
  },
};
