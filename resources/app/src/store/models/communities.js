import Vue from "vue";
import RestModule from "../RestModule";

export default new RestModule(
  "communities",
  {
    exportFields: ["id", "name", "type", "center", "area"],
  },
  {
    async addUser({ commit }, { id, data }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const response = await Vue.axios.post(`/communities/${id}/users`, data, {
          params: {
            fields: "*,communities.*",
          },
          cancelToken: cancelToken.token,
        });
        commit("users/addData", [response.data], { root: true });
        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);
        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
    async removeUser({ commit }, { id, userId }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        await Vue.axios.delete(`/communities/${id}/users/${userId}`, {
          cancelToken: cancelToken.token,
        });

        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);

        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
    async updateUser({ commit, rootState }, { id, userId, data }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        await Vue.axios.put(`/communities/${id}/users/${userId}`, data, {
          params: {
            fields: "*,communities.*",
          },
          cancelToken: cancelToken.token,
        });
        commit("cancelToken", null);

        const userIndex = rootState.users.data.findIndex((u) => u.id === data.id);
        const newUserArray = [...rootState.users.data];
        newUserArray[userIndex] = data;

        commit("users/data", newUserArray, { root: true });
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);
        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
  }
);
