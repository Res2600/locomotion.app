import RestModule from "../RestModule";

export default new RestModule("padlocks", {
  exportFields: ["id", "external_id", "name", "mac_address", "loanable.id", "loanable.name"],
});
