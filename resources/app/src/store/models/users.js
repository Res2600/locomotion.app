import Vue from "vue";
import { extractErrors } from "@/helpers";
import RestModule from "../RestModule";

export default new RestModule(
  "users",
  {
    exportFields: [
      "id",
      "created_at",
      "email",
      "last_name",
      "name",
      "phone",
      "balance",
      "postal_code",
      "borrower.approved_at",
      "communities.id",
      "communities.name",
      "communities.approved_at",
      "communities.suspended_at",
    ],
  },
  {
    async approveBorrower({ commit }, userId) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const { data } = await Vue.axios.put(`/users/${userId}/borrower/approve`, null, {
          cancelToken: cancelToken.token,
        });

        commit("mergeItem", { borrower: data });

        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);

        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
    async joinCommunity({ commit }, { communityId, userId }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const response = await Vue.axios.put(`/users/${userId}/communities/${communityId}`, null, {
          cancelToken: cancelToken.token,
        });

        commit("mergeItem", { communities: [response.data] });

        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);

        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
    async suspendBorrower({ commit }, userId) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const { data } = await Vue.axios.put(`/users/${userId}/borrower/suspend`, null, {
          cancelToken: cancelToken.token,
        });

        commit("mergeItem", { borrower: data });

        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);

        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
    async unsuspendBorrower({ commit }, userId) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const { data } = await Vue.axios.delete(`/users/${userId}/borrower/suspend`, {
          cancelToken: cancelToken.token,
        });

        commit("mergeItem", { borrower: data });

        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);

        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
    async updateEmail({ commit }, { userId, currentPassword, newEmail }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);

        // request to update email
        const { data } = await Vue.axios.post(
          `/users/${userId}/email`,
          {
            password: currentPassword,
            email: newEmail,
          },
          { cancelToken: cancelToken.token }
        );

        commit("mergeItem", data);
        commit("cancelToken", null);

        return data;
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);
        const { request } = e;

        if (request) {
          switch (request.status) {
            // notify user that current password is invalid
            case 401:
              commit(
                "addNotification",
                {
                  content: "Le mot de passe actuel est invalide.",
                  title: "Erreur d'authentification",
                  variant: "danger",
                  type: "password",
                },
                { root: true }
              );
              return;
            case 422:
              commit(
                "addNotification",
                {
                  content: extractErrors(e.response.data).join(", "),
                  title: "Erreur de changement",
                  variant: "danger",
                  type: "email",
                },
                { root: true }
              );
              return;
            default:
              break;
          }
        }
        throw e;
      }
    },
    async updatePassword({ commit }, { userId, currentPassword, newPassword }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);

        // request to update password
        const response = await Vue.axios.post(
          `/users/${userId}/password`,
          {
            current: currentPassword,
            new: newPassword,
          },
          { cancelToken: cancelToken.token }
        );

        commit("cancelToken", null);

        return response;
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);

        const { request } = e;

        if (request) {
          switch (request.status) {
            // notify user that current password is invalid
            case 401:
              commit(
                "addNotification",
                {
                  content: "Le mot de passe actuel est invalide.",
                  title: "Erreur d'authentification",
                  variant: "danger",
                  type: "password",
                },
                { root: true }
              );
              return;
            default:
              break;
          }
        }

        throw e;
      }
    },
    async submitBorrower({ commit, state, rootState }) {
      commit("loaded", false);
      commit("loading", true);
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const { data: borrower } = await Vue.axios.put(
          `/users/${state.item.id}/borrower/submit`,
          state.item.borrower,
          {
            cancelToken: cancelToken.token,
          }
        );

        if (rootState.user.id === state.item.id) {
          commit("mergeUser", { borrower }, { root: true });
        }
        commit("patchItem", { borrower });
        commit("patchInitialItem", { borrower });

        commit("loaded", true);
        commit("loading", false);

        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);
        commit("loading", false);

        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
    async update({ commit, state, dispatch, rootState }, { id, data, params }) {
      commit("loaded", false);
      commit("loading", true);
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);

        const { data: item } = await dispatch("sendRequest", {
          method: "put",
          url: `/${state.slug}/${id}`,
          data,
          params: {
            ...params,
          },
        });

        // If the user currently being updated is the logged-in user
        // (rootState.user), then update it's state as well.
        // Only diff with the update method in RestModule.
        if (rootState.user.id === item.id) {
          commit("mergeUser", { ...item }, { root: true });
        }
        commit("item", item);
        commit("initialItem", item);

        commit("loaded", true);
        commit("loading", false);
      } catch (e) {
        if (!Vue.axios.isCancel(e)) {
          commit("loading", false);
        }
        throw e;
      }
    },
  }
);
