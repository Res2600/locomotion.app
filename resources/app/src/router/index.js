import Vue from "vue";
import VueRouter from "vue-router";

import NotFound from "@/views/NotFound.vue";

import adminRoutes from "@/router/admin";
import authRoutes from "@/router/auth";
import baseRoutes from "@/router/base";
import communityRoutes from "@/router/community";
import loanableRoutes from "@/router/loanables";
import loanRoutes from "@/router/loans";
import profileRoutes from "@/router/profile";
import registerRoutes from "@/router/register";
import { persistDevParam } from "@/helpers/devMode";

Vue.use(VueRouter);

const routes = [
  ...adminRoutes,
  ...authRoutes,
  ...baseRoutes,
  ...communityRoutes,
  ...loanableRoutes,
  ...loanRoutes,
  ...profileRoutes,
  ...registerRoutes,
  {
    path: "/404",
    component: NotFound,
    meta: {},
  },
  { path: "*", redirect: "/404" },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    // If we navigate using back, forward.
    if (savedPosition) {
      // This doesn't work too well when we load new data. Ideally this would hook
      // up somehow with the data route guards, or the form mixin, to only navigate once
      // data is loaded
      return savedPosition;
    }

    // If we navigate to a new page, go to top
    if (to.matched.length >= 1 && to.path !== from.path) {
      return { x: 0, y: 0 };
    }

    // If we stay on the same page, do not navigate. (e.g. if we change the query params)
    return undefined;
  },
});

router.beforeEach((to, from, next) => {
  const {
    body: { style },
  } = document;

  persistDevParam(to, from, next);
});

export default router;
