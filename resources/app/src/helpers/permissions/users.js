function isGlobalAdmin(user) {
  return user.role === "admin";
}

function userIsSame(userA, userB) {
  return userA && userB && userA.id === userB.id;
}

function canChangeUserPassword(accessingUser, accessedUser) {
  if (!accessingUser) {
    return false;
  }

  // One can change their own password.
  if (userIsSame(accessingUser, accessedUser)) {
    return true;
  }

  // Global admins can too.
  return isGlobalAdmin(accessingUser);
}

function canEditUser(accessingUser, accessedUser) {
  if (!accessingUser) {
    return false;
  }

  // One can change their own dtails.
  if (userIsSame(accessingUser, accessedUser)) {
    return true;
  }

  return isGlobalAdmin(accessingUser);
}

function canEditDriversProfile(accessingUser, accessedUser) {
  if (!accessingUser) {
    return false;
  }

  if (userIsSame(accessingUser, accessedUser)) {
    return true;
  }

  return isGlobalAdmin(accessingUser);
}

function canManageGlobalAdmins(accessingUser) {
  return accessingUser && isGlobalAdmin(accessingUser);
}

function canSeeDeletedUser(accessingUser) {
  return isGlobalAdmin(accessingUser);
}

export {
  isGlobalAdmin,
  userIsSame,
  canChangeUserPassword,
  canEditDriversProfile,
  canEditUser,
  canManageGlobalAdmins,
  canSeeDeletedUser,
};
