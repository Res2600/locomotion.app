<tr style="height: 325px">
    <td
        style="
            background-position: bottom center;
            background-repeat: no-repeat;
            background-color: #1e4847;
            height: 263.27px;
            background-image: url('{{ env('BACKEND_URL_FROM_BROWSER') }}/mail-footer-bg.png');    
        "
        align="top"
    >
        <p
            style="
                position: relative;
                margin: 0;
                color: white;
                font-weight: bold;
                font-size: 13px;
                line-height: 16px;
                text-align: center;
            "
        >
            Envoyé par l'équipe LocoMotion
        </p>
        <p
            style="
                position: relative;
                margin: 0;
                color: white;
                font-weight: 390;
                font-size: 13px;
                line-height: 16px;
                text-align: center;
            "
        >
            info@locomotion.app
        </p>
    </td>
</tr>
