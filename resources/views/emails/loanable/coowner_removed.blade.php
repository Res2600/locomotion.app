@extends('emails.layouts.main') @section('content')
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
        margin-bottom: 32px;
    "
>
    Bonjour {{ $coowner->user->name }},
</p>

<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    {{ $remover->name }} a retiré vos droits de gestion du véhicule {{
    $coowner->loanable->name }}. Vous n'avez maintenant plus accès à la
    modification des informations du véhicule, à la modification de ses
    disponibilités et à la gestion de ses emprunts.
</p>

<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    S'il s'agit d'une erreur, contactez {{ $remover->full_name }} directement au
    {{ $remover->phone }}.
</p>
@endsection
