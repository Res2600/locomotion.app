@extends('emails.layouts.main_text') @section('content')
    Bonjour {{ $added->name }},

    {{ $adder->name }} vous a donné les droits de gestion du véhicule
    {{ $loanable->name }}! Vous pouvez maintenant modifier les informations du
    véhicule, ses disponibilités et gérer ses emprunts.

    Voir le véhicule: [{{ env('FRONTEND_URL') . '/profile/loanables/' . $loanable->id }}]

@endsection
