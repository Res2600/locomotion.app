@extends('emails.layouts.main_text') @section('content')
        Bonjour {{ $coowner->user->name }},

        {{ $remover->name }} a retiré vos droits de gestion du véhicule
        {{$coowner->loanable->name }}. Vous n'avez maintenant plus accès à la modification des informations du
        véhicule, à la modification de ses disponibilités et à la gestion de ses emprunts.

        S'il s'agit d'une erreur, contactez {{ $remover->full_name }} directement au {{ $remover->phone }}.
@endsection
