@extends('emails.layouts.main_text')

@section('content')
Bonjour {{ $borrower->user->name }},

Votre demande d'emprunt de {{ $loan->loanable->name }} de {{$owner->user->name }}
à partir de {{ $loan->departure_at }} a été acceptée!

{{ $intention->message_for_borrower }}

Voir l'emprunt [{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}]

            - L'équipe LocoMotion
@endsection
