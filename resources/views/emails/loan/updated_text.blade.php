@extends('emails.layouts.main_text')

@section('content')
    Bonjour {{ $recipient->name }},

    @if($isUpdaterBorrower)
        {{ $updater->name }} a mis à jour son emprunt du véhicule {{ $loan->loanable->name }}
        à partir de {{ $loan->departure_at }} et pour un total de {{ $loan->duration_in_minutes }} minutes.
    @else
        {{ $updater->name }} a mis à jour l'emprunt du véhicule {{ $loan->loanable->name }} par
        {{ $loan->borrower->user->name }} à partir de {{ $loan->departure_at }} et pour un total de {{ $loan->duration_in_minutes }} minutes.
    @endif

    {{ $loan->message_for_owner }}

    Voir l'emprunt [{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}] pour l'approuver ou le refuser.

    - L'équipe LocoMotion
@endsection
