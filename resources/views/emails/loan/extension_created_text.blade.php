@extends('emails.layouts.main_text')

@section('content')
Bonjour {{ $recipient->name }},

{{ $borrower->user->name }} a demandé à rallonger son emprunt du véhicule {{ $loan->loanable->name }} qui commençait à {{ $loan->departure_at }} et qui durerait maintenant {{ $extension->new_duration }} minutes.

{{ $extension->comments_on_extension }}

Voir l'emprunt [{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}]

            - L'équipe LocoMotion
@endsection
