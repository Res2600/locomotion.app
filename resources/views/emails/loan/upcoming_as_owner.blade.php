@extends('emails.layouts.main') @section('content')
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
        margin-bottom: 32px;
    "
>
    Bonjour {{ $user->name }},
</p>

<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    L'emprunt de {{$loan->loanable->name}} par {{ $borrowerUser->name }}
    commence dans 3 heures environ, veuillez prendre connaissance de la marche à
    suivre si ce n'est pas déjà fait.
</p>

<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Si le véhicule n'est plus disponible, communiquez directement avec
    l'emprunteur et annulez l'emprunt en ligne
    <a href="{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}">ici</a>.
</p>

<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Merci de votre participation!
</p>

@endsection
