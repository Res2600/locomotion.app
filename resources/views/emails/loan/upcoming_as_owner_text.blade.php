@extends('emails.layouts.main_text') @section('content')
    Bonjour {{ $user->name }},

    L'emprunt de {{$loan->loanable->name}} par {{ $borrowerUser->name }} commence dans 3
    heures environ, veuillez prendre connaissance de la marche à suivre si ce
    n'est pas déjà fait.

    Si le véhicule n'est plus disponible, communiquez directement avec
    l'emprunteur et annulez l'emprunt en ligne ici
    [{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}].

    Merci de votre participation!

    - L'équipe LocoMotion

@endsection
