@extends('emails.layouts.main') @section('content')
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
        margin-bottom: 32px;
    "
>
    Bonjour {{ $recipient->name }},
</p>

@if($isBorrower)
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    {{ $validator->name }} a accepté les données de la fin de votre emprunt de
    {{$loan->loanable->name}}.
</p>
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Vous pouvez maintenant valider les données et payer {{
    $loan->loanable->owner->user->name }}. Si des données sont incorrectes, vous
    pouvez contester pour qu'un membre de l'équipe LocoMotion contacte les
    participant-e-s et ajuste les données.
</p>

<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Si vous ne validez ni ne contestez les données de l'emprunt, celui-ci sera
    validé et payé automatiquement depuis votre solde 48 heures après la fin de
    la réservation.
</p>

@else
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    {{ $validator->name }} a accepté les données à la fin de l'emprunt du
    véhicule {{ $loan->loanable->name }} et a authorisé le paiement.
</p>
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Vous pouvez maintenant accepter les fonds en validant les données. Si des
    données sont incorrectes, vous pouvez contester pour qu'un membre de
    l'équipe LocoMotion contacte les participant-e-s et ajuste les données.
</p>
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Si vous ne validez ni ne contestez les données de l'emprunt, celui-ci sera
    validé et payé automatiquement 48 heures après la fin de la réservation.
</p>
@endif

<p style="text-align: center; margin: 32px auto 0 auto">
    <a
        href="{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}"
        style="
            display: inline-block;
            background-color: #246aea;
            padding: 8px 16px;
            border-radius: 5px;
            color: white;
            font-weight: bold;
            font-size: 17px;
            line-height: 24px;
            text-decoration: none;
        "
        target="_blank"
        >Voir l'emprunt</a
    >
</p>

@endsection
