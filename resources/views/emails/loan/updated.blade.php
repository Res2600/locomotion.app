@extends('emails.layouts.main') @section('content')
<p
    style="
        text-align: center;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
        margin-bottom: 32px;
    "
>
    Bonjour {{ $recipient->name }},
</p>

@if($isUpdaterBorrower)

<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    {{ $updater->name }} a mis à jour son emprunt du véhicule {{
    $loan->loanable->name }} à partir de {{ $loan->departure_at }} et pour un
    total de {{ $loan->duration_in_minutes }} minutes.
</p>
@else
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    {{ $updater->name }} a mis à jour l'emprunt du véhicule {{
    $loan->loanable->name }} par {{ $loan->borrower->user->name }} à partir de
    {{ $loan->departure_at }} et pour un total de {{ $loan->duration_in_minutes
    }} minutes.
</p>
@endif

<br />

@if (!!$loan->message_for_owner)
<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Commentaires:
</p>

<p
    style="
        text-align: center;
        margin-top: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    {{ $loan->message_for_owner }}
</p>
@endif

<p style="text-align: center; margin: 32px auto 0 auto">
    <a
        href="{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}"
        style="
            display: inline-block;
            background-color: #246aea;
            padding: 8px 16px;
            border-radius: 5px;
            color: white;
            font-weight: bold;
            font-size: 17px;
            line-height: 24px;
            text-decoration: none;
        "
        target="_blank"
        >Voir l'emprunt pour l'approuver ou le refuser.</a
    >
</p>

@endsection
