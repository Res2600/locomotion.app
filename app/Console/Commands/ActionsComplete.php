<?php

namespace App\Console\Commands;

use App\Http\Controllers\LoanController;
use App\Http\Controllers\PaymentController;
use App\Models\Loan;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Log;

class ActionsComplete extends Command
{
    protected $signature = "actions:complete";

    protected $description = "Complete actions after 48 hours of inactivity";

    public function handle()
    {
        Log::info("Starting actions autocompletion command...");

        $loanExpirationTime = CarbonImmutable::now()->subHours(48);
        $loans = self::getActiveLoansScheduledToReturnBefore(
            $loanExpirationTime
        );

        foreach ($loans as $loan) {
            // Repair any inconsistent loan.
            LoanController::loanActionsForward($loan);

            // Autocomplete nothing if loan is contested
            if (
                ($loan->handover && $loan->handover->isContested()) ||
                ($loan->takeover && $loan->takeover->isContested())
            ) {
                continue;
            }

            // Cancel all ongoing extensions
            foreach ($loan->extensions as $extension) {
                if ($extension->status === "in_process") {
                    Log::info("Canceling extension on loan ID $loan->id...");

                    $extension->cancel();
                    $extension->save();

                    Log::info("Canceled extension on loan ID $loan->id.");
                }
            }

            if ($loan->intention && $loan->intention->status === "in_process") {
                self::cancelLoan($loan, "intention");
                continue;
            }

            if (
                $loan->prePayment &&
                $loan->prePayment->status === "in_process"
            ) {
                self::cancelLoan($loan, "pre_payment");
                continue;
            }

            if ($loan->takeover && $loan->takeover->status === "in_process") {
                self::cancelLoan($loan, "takeover");
                continue;
            }

            /*
              Handovers:

              Complete handover if takeover is not contested
            */
            if ($loan->handover && $loan->handover->status === "in_process") {
                Log::info("Autocompleting handover on loan ID $loan->id...");

                $loan->handover->mileage_end =
                    $loan->takeover->mileage_beginning +
                    $loan->estimated_distance;
                $loan->handover->purchases_amount = 0;
                $loan->handover->complete();
                $loan->handover->save();
                $loan->handover->refresh();
                LoanController::loanActionsForward($loan);

                Log::info("Completed handover on loan ID $loan->id.");
            }

            // Need to refresh since we check the state of the loan before paying.
            $loan->refresh();
            if ($loan->borrower->user->can("pay", $loan)) {
                Log::info("Autocompleting payment on loan ID $loan->id...");
                PaymentController::pay($loan, true);
                Log::info("Completed payment on loan ID $loan->id.");
            } else {
                Log::info("Not autocompleting payment on loan ID $loan->id.");
            }
        }

        Log::info("Completed actions autocompletion command.");
    }

    public static function getActiveLoansScheduledToReturnBefore($datetime)
    {
        return Loan::where("status", "=", "in_process")
            ->where("actual_return_at", "<=", $datetime)
            ->get();
    }

    /**
     * Cancels the loan and logs which action was ongoing.
     * @param string $action Ongoing action
     */
    public static function cancelLoan(Loan $loan, string $action): void
    {
        Log::info("Autocancelling loan ID $loan->id on $action action...");
        $loan->cancel()->save();
        Log::info("Canceled loan ID $loan->id.");
    }
}
