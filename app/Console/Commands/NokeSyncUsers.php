<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\NokeService;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Log;

class NokeSyncUsers extends Command
{
    protected $signature = 'noke:sync:users
                            {--pretend : Do not call remote API}';

    protected $description = "Synchronize NOKE users";

    private $pretend = false;

    public function __construct(Client $client, NokeService $service)
    {
        parent::__construct();

        $this->client = $client;
        $this->service = $service;
    }

    public function handle()
    {
        Log::info("Fetching users...");
        $this->getNokeUsers(true);

        Log::info("Creating remote users...");
        $this->createUsers();

        Log::info("Done.");
    }

    private function getNokeUsers()
    {
        $this->nokeUsers = $this->service->fetchUsers();

        foreach ($this->nokeUsers as $user) {
            // Noke's username is email address.
            $this->nokeUsersIndex[$user->username] = $user;
        }
    }

    public static function getLocalUsers()
    {
        return User::whereHas("borrower")
            ->whereHas("approvedCommunities")

            ->whereHas("communities", function ($q) {
                return $q->where("uses_noke", true);
            })

            ->select(
                "id",
                "email",
                "name",
                "last_name",
                "phone",
                "is_smart_phone"
            )
            ->get();
    }

    private function createUsers()
    {
        $users = $this->getLocalUsers();
        foreach ($users as $user) {
            if (!isset($this->nokeUsersIndex[$user->email])) {
                Log::info("Creating user {$user->email}.");

                if ($this->pretend) {
                    continue;
                }

                $this->service->findOrCreateUser($user);
            }
        }
    }
}
