<?php

namespace App\Console\Commands;

use App\Mail\Loan\Upcoming as LoanUpcoming;
use App\Mail\Loan\UpcomingAsOwner;
use App\Mail\UserMail;
use App\Models\Loan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Log;

class EmailLoanUpcoming extends Command
{
    protected $signature = 'email:loan:upcoming
                            {--pretend : Do not send emails}';

    protected $description = "Send loan upcoming emails (in three hours)";

    private $pretend = false;

    public function handle()
    {
        if ($this->option("pretend")) {
            $this->pretend = true;
        }

        Log::info(
            "Fetching loans starting in three hours or less created at least three hours before now..."
        );

        $query = $this->getQuery();

        $loans = $query->cursor();
        foreach ($loans as $loan) {
            if (
                !$loan->loanable->is_self_service &&
                $loan->intention?->status !== "completed"
            ) {
                // Do not send messages for on-demand loanables which haven't been explicitly approved.
                continue;
            }

            $borrowerUser = $loan->borrower->user;
            if (!$this->pretend) {
                Log::info(
                    "Sending LoanUpcoming email to borrower at: $borrowerUser->email"
                );

                UserMail::queue(
                    new LoanUpcoming($borrowerUser, $loan),
                    $borrowerUser
                );

                // Loanable has an owner and is not self service.

                if (
                    $loan->loanable->owner &&
                    !$loan->loanable->is_self_service
                ) {
                    Log::info(
                        "Sending LoanUpcoming email to owners of loanable {$loan->loanable->id}"
                    );

                    $loan->loanable->queueMailToOwners(function (
                        User $user
                    ) use ($loan, $borrowerUser) {
                        return new UpcomingAsOwner($user, $loan, $borrowerUser);
                    }, $borrowerUser);
                }

                $meta = $loan->meta;
                $meta["sent_loan_upcoming_email"] = true;
                $loan->meta = $meta;

                $loan->save();
            } else {
                Log::info(
                    "Would have sent LoanUpcoming email to borrower at: {$borrowerUser->email}" .
                        " for loan with id: {$loan->id}"
                );

                // Loanable has an owner and is not self service.
                if (
                    $loan->loanable->owner &&
                    !$loan->loanable->is_self_service
                ) {
                    $ownerUser = $loan->loanable->owner->user;
                    Log::info(
                        "Would have sent LoanUpcoming email to owner at: {$ownerUser->email} " .
                            "for loan with id: {$loan->id}"
                    );
                }
            }
        }

        Log::info("Done.");
    }

    public static function getQuery()
    {
        $now = Carbon::now();
        $threeHoursAgo = $now->copy()->subtract(3, "hours");
        $inThreeHours = $now->copy()->add(3, "hours");

        $query = Loan::where("status", "=", "in_process")
            ->where("departure_at", "<=", $inThreeHours)
            ->where("departure_at", ">", $now)
            ->where("loans.created_at", "<=", $threeHoursAgo)
            ->where("meta->sent_loan_upcoming_email", null);

        return $query;
    }
}
