<?php

namespace App\Transformers;

use App\Models\Loan;
use App\Models\Loanable;
use Auth;

class CarTransformer extends LoanableTransformer
{
    public function transform($options = [])
    {
        $output = parent::transform($options);

        $user = Auth::user();

        /** @var ?Loanable $loanable */
        $loanable = $this->getFirstAncestor(Loanable::class);
        if (
            $user &&
            ($user->isAdmin() ||
                ($loanable && $loanable->isOwnerOrCoowner($user)))
        ) {
            return $output;
        }

        $publicFields = [
            "id",
            "brand",
            "model",
            "year_of_circulation",
            "transmission_mode",
            "engine",
        ];

        if ($this->hasAncestor(Loan::class)) {
            $publicFields[] = "papers_location";
        }

        return $this->filterKeys($output, $publicFields);
    }
}
