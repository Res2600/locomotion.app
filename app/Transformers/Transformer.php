<?php

namespace App\Transformers;

use App\Models\AuthenticatableBaseModel;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Transformer
{
    protected array $pivots = [];

    public function __construct(
        protected Model $item,
        protected ?Transformer $parent = null
    ) {
    }

    public static function for(
        Model $item,
        Transformer $parent = null
    ): Transformer {
        return new self($item, $parent);
    }

    protected function hasAncestor(string $type): bool
    {
        if (!$this->parent) {
            return false;
        }
        if (get_class($this->parent->item) === $type) {
            return true;
        }
        return $this->parent->hasAncestor($type);
    }

    protected function getFirstAncestor(string $type): ?Model
    {
        if (!$this->parent) {
            return null;
        }
        if (get_class($this->parent->item) === $type) {
            return $this->parent->item;
        }
        return $this->parent->getFirstAncestor($type);
    }

    protected function authorize($output, $options)
    {
        return $output;
    }

    public function transform($options = [])
    {
        $fields = array_get($options, "fields", []);
        if (!$fields || is_string($fields)) {
            $fields = ["*" => "*"];
        }

        if (in_array("*", array_keys($fields))) {
            $computedFields = array_diff(
                $this->item->computed,
                array_keys(array_get($options, "!fields", []))
            );
        } else {
            $computedFields = array_intersect(
                array_keys($fields),
                $this->item->computed
            );
        }
        $this->item->append($computedFields);
        $output = $this->item->toArray();

        $this->addItems($output, $options);
        $this->addCollections($output, $options);
        foreach ($this->pivots as $pivot) {
            if ($this->hasAncestor($pivot) && $options["pivot"]) {
                $this->includePivotsInOutput($output, $options);
            }
        }

        if ($this->applyFieldsOption($options)) {
            $output = array_intersect_key(
                $output,
                array_merge(array_flip(array_keys($options["fields"])), [
                    "id" => true,
                ])
            );
        }

        unset($output["pivot"]);
        unset($output["laravel_through_key"]);

        return $this->authorize($output, $options);
    }

    private function addCollections(&$output, &$options): void
    {
        foreach (
            array_merge(
                $this->item->collections,
                array_keys($this->item->morphManys)
            )
            as $relation
        ) {
            $camelRelation = Str::camel($relation);
            if (
                $this->shouldIncludeRelation($relation, $this->item, $options)
            ) {
                $target = $this->item->{$camelRelation};

                $output[$relation] = $target->map(function (
                    BaseModel|AuthenticatableBaseModel $childItem
                ) use ($options, $relation) {
                    $relationFields = [];
                    if (
                        isset($options["fields"][$relation]) &&
                        $relation !== $options["fields"][$relation]
                    ) {
                        $relationFields = $options["fields"][$relation];
                    }

                    $notRelationFields = [];
                    if (
                        isset($options["fields"]["!$relation"]) &&
                        $relation !== $options["fields"]["!$relation"]
                    ) {
                        $notRelationFields = $options["fields"]["!$relation"];
                    }

                    return $childItem->getTransformer($this)->transform([
                        "fields" => $relationFields,
                        "!fields" => $notRelationFields,
                        "pivot" => $childItem->pivot,
                    ]);
                });
            }
        }
    }

    private function addItems(&$output, &$options): void
    {
        foreach (
            array_merge($this->item->items, array_keys($this->item->morphOnes))
            as $relation
        ) {
            if (
                $this->shouldIncludeRelation($relation, $this->item, $options)
            ) {
                $camelRelation = Str::camel($relation);
                /** @var BaseModel|AuthenticatableBaseModel $childItem */
                $childItem = $this->item->{$camelRelation};
                if (!$childItem) {
                    $output[$relation] = null;
                    continue;
                }

                $transformer = $childItem->getTransformer($this);
                $output[$relation] = $transformer->transform([
                    "fields" => $options["fields"][$relation] ?? [],
                    "!fields" => $options["fields"]["!$relation"] ?? [],
                    "pivot" => $this->item->pivot,
                ]);
            }
        }
    }

    protected static function shouldIncludeRelation(
        $relation,
        &$item,
        $options
    ): bool {
        return isset($options["fields"]) &&
            (in_array(
                $relation,
                self::wrapArrayKeys($options["fields"]),
                true
            ) ||
                isset($options["fields"]["*"]["*"]) ||
                in_array($relation, $item->getWith(), true));
    }

    protected static function shouldIncludeField($field, $options): bool
    {
        return (!isset($options["fields"]) ||
            in_array($field, self::wrapArrayKeys($options["fields"]), true) ||
            in_array("*", self::wrapArrayKeys($options["fields"]), true)) &&
            (!isset($options["!fields"]) ||
                !in_array(
                    $field,
                    self::wrapArrayKeys($options["!fields"]),
                    true
                ));
    }

    protected static function applyFieldsOption($options): bool
    {
        return array_key_exists("fields", $options) &&
            is_array($options["fields"]) &&
            !empty($options["fields"]) &&
            !in_array("*", array_keys($options["fields"]));
    }

    protected function includePivotsInOutput(&$output, $options): void
    {
        foreach ($options["pivot"]->toArray() as $key => $value) {
            if ($key === "id") {
                continue;
            }

            if ($this->shouldIncludeField($key, $options)) {
                $output[$key] = $options["pivot"][$key];
            }
        }

        foreach (
            array_merge(
                $options["pivot"]->items,
                array_keys($options["pivot"]->morphOnes)
            )
            as $relation
        ) {
            if (
                $this->shouldIncludeRelation(
                    $relation,
                    $options["pivot"],
                    $options
                )
            ) {
                $output[$relation] = $options["pivot"]->{$relation};
            }
        }

        foreach (
            array_merge(
                $options["pivot"]->collections,
                array_keys($options["pivot"]->morphManys)
            )
            as $relation
        ) {
            if (
                $this->shouldIncludeRelation(
                    $relation,
                    $options["pivot"],
                    $options
                )
            ) {
                $output[$relation] = $options["pivot"]->{$relation};
            }
        }
    }

    public static function filterKeys($output, $keys): array
    {
        return array_intersect_key($output, array_flip($keys));
    }

    public static function wrapArrayKeys($value): array
    {
        if (!is_array($value)) {
            return [$value];
        }
        return array_keys($value);
    }
}
