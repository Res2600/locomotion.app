<?php

namespace App\Transformers;

use App\Models\User;
use Auth;

class CommunityTransformer extends Transformer
{
    protected array $pivots = [User::class];

    public function transform($options = [])
    {
        $output = parent::transform($options);

        $user = Auth::user();
        if ($user && $user->isAdmin()) {
            return $output;
        }

        if (!$user) {
            unset($output["users"]);
        }

        $approvedCommunity = $user->communities
            ->where("id", $this->item->id)
            ->where("pivot.approved_at", "!=", null);
        if ($approvedCommunity->isEmpty()) {
            unset($output["users"]);
        }

        return $output;
    }
}
