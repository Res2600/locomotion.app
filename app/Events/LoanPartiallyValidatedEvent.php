<?php

namespace App\Events;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;

class LoanPartiallyValidatedEvent
{
    use Dispatchable;

    /**
     * @var Loan
     */
    public $loan;
    /**
     * @var User
     */
    public $validator;

    public function __construct(Loan $loan, User $validator)
    {
        $this->loan = $loan;
        $this->validator = $validator;
    }
}
