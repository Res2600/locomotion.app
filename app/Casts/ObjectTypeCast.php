<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

// TODO(#1148): Delete this when moving the pricing rules to the community_loanable_types table
class ObjectTypeCast implements CastsAttributes
{
    public function set($model, $key, $value, $attributes)
    {
        return match ($value) {
            "car" => "App\Models\Car",
            "trailer" => "App\Models\Trailer",
            "bike" => "App\Models\Bike",
            default => $value,
        };
    }

    public function get($model, $key, $value, $attributes)
    {
        return match ($value) {
            "App\Models\Car" => "car",
            "App\Models\Trailer" => "trailer",
            "App\Models\Bike" => "bike",
            default => $value,
        };
    }
}
