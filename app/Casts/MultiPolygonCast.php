<?php

namespace App\Casts;

use GeoJson\Geometry\MultiPolygon as GeoMultiPolygon;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use MStaack\LaravelPostgis\Geometries\LineString;
use MStaack\LaravelPostgis\Geometries\MultiPolygon as GisMultiPolygon;
use MStaack\LaravelPostgis\Geometries\Point;
use MStaack\LaravelPostgis\Geometries\Polygon;

// Because of
// https://stackoverflow.com/questions/7309121/preferred-order-of-writing-latitude-longitude-tuples
class MultiPolygonCast implements CastsAttributes
{
    public function set($model, $key, $value, $attributes)
    {
        if (!$value) {
            return null;
        }

        // Used in seeding
        if (is_string($value)) {
            return $value;
        }

        // The GeoJson constructor can actually take plain old arrays, while
        // the postGist constructor expects an array of typed objects.
        if (is_array($value)) {
            $value = new GeoMultiPolygon($value["coordinates"] ?? $value);
        }

        if (is_a($value, GeoMultiPolygon::class)) {
            return self::toPostGis($value);
        }

        throw new \Exception("invalid");
    }

    public function get($model, $key, $value, $attributes)
    {
        if (!$value) {
            return null;
        }

        if (is_a($value, GisMultiPolygon::class)) {
            return $value->jsonSerialize();
        }

        if (is_array($value)) {
            return new GeoMultiPolygon($value);
        }

        throw new \Exception("invalid");
    }

    public static function toPostGis(GeoMultiPolygon $value): GisMultiPolygon
    {
        return new GisMultiPolygon(
            array_map(
                fn($polygon) => new Polygon(
                    array_map(
                        fn($lineString) => new LineString(
                            array_map(
                                // GeoJSON is Lng,Lat; GIS is opposite!
                                fn($point) => new Point($point[1], $point[0]),
                                $lineString
                            )
                        ),
                        $polygon
                    )
                ),
                $value->getCoordinates()
            )
        );
    }
}
