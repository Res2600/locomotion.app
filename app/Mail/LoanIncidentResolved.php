<?php

namespace App\Mail;

use App\Models\Incident;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanIncidentResolved extends BaseMailable
{
    use Queueable, SerializesModels;

    public $incident;
    public $loan;
    public $recipient;

    public function __construct(Incident $incident, Loan $loan, User $recipient)
    {
        $this->incident = $incident;
        $this->loan = $loan;
        $this->recipient = $recipient;
    }

    public function build()
    {
        return $this->view("emails.loan.incident_resolved")
            ->subject("LocoMotion - Incident résolu")
            ->text("emails.loan.incident_resolved_text")
            ->with([
                "title" => "Incident résolu",
            ]);
    }
}
