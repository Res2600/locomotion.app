<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Updated extends BaseMailable
{
    use Queueable, SerializesModels;

    public $isUpdaterBorrower;

    public function __construct(
        public User $updater,
        public User $recipient,
        public Loan $loan
    ) {
        $this->isUpdaterBorrower = $this->loan->borrower->user->is(
            $this->updater
        );
    }

    public function build()
    {
        return $this->view("emails.loan.updated")
            ->subject("LocoMotion - Emprunt mis à jour")
            ->text("emails.loan.updated_text")
            ->with([
                "title" => "Emprunt mis à jour",
            ]);
    }
}
