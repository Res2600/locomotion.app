<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class InvoicePaid extends BaseMailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $formattedAddress;

    public function __construct(
        public User $user,
        public array $invoice,
        public $title = null,
        public $text = null,
        $subject = null
    ) {
        $this->subject = $subject ?: "Locomotion - $this->title";
        $this->formattedAddress = $this->formatAddress();
    }

    public function build()
    {
        return $this->view("emails.invoice.paid")
            ->subject($this->subject)
            ->text("emails.invoice.paid_text");
    }

    private function formatAddress(): array
    {
        return explode(",", $this->user->address);
    }
}
