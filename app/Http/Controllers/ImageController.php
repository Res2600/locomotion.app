<?php

namespace App\Http\Controllers;

use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Image;
use App\Repositories\ImageRepository;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Exception\NotSupportedException;
use Intervention\Image\ImageManager as ImageManager;

class ImageController extends FileController
{
    protected $imageableId;

    protected $types = ["user"];

    public function __construct(ImageRepository $image)
    {
        $this->repo = $image;
    }

    protected function upload($file, $field)
    {
        $uniq = uniqid();
        $uri = "/storage/tmp/$uniq";

        $originalFilename = $file->getClientOriginalName();
        $filename = $this->cleanupFilename($originalFilename);

        $manager = new ImageManager(["driver" => "imagick"]);
        try {
            $image = $manager->make($file)->orientate();
        } catch (NotReadableException) {
            return ErrorResponse::withMessage("Fichier illisible.", 422);
        }

        try {
            Image::store($uri . DIRECTORY_SEPARATOR . $filename, $image);
        } catch (NotSupportedException) {
            return ErrorResponse::withMessage("Format non supporté.", 422);
        }

        $request = new Request();
        $request->merge([
            "path" => $uri,
            "original_filename" => $originalFilename,
            "filename" => $filename,
            "width" => $image->width(),
            "height" => $image->height(),
            "field" => $field,
            "filesize" => $image->filesize(),
        ]);

        return $request->input();
    }
}
