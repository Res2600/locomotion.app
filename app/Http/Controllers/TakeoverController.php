<?php

namespace App\Http\Controllers;

use App\Events\LoanTakeoverContestationResolvedEvent;
use App\Events\LoanTakeoverContestedEvent;
use App\Http\Controllers\LoanController;
use App\Http\Requests\Action\TakeoverRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Loan;
use App\Models\Takeover;
use App\Repositories\LoanRepository;
use App\Repositories\RestRepository;
use App\Repositories\TakeoverRepository;
use Gate;

class TakeoverController extends RestController
{
    public function __construct(LoanRepository $repository, Takeover $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function complete(TakeoverRequest $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);
        Gate::authorize("completeTakeover", $loan);

        $takeover = $loan->takeover;
        $wasContested = $takeover->isContested();
        $takeover->fill($request->all());
        $takeover->comments_on_contestation = "";
        $takeover->complete();

        RestRepository::saveItemAndRelations($takeover, $request->all());

        $takeover->refresh();

        // Move forward if possible.
        LoanController::loanActionsForward($takeover->loan);

        if ($wasContested) {
            event(
                new LoanTakeoverContestationResolvedEvent(
                    $takeover,
                    $request->user()
                )
            );
        }

        return $takeover;
    }

    public function contest(Request $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);
        Gate::authorize("contestTakeoverInfo", $loan);

        $takeover = $loan->takeover;
        $takeover->comments_on_contestation = $request->get(
            "comments_on_contestation"
        );
        $takeover->contest();
        $takeover->save();

        event(new LoanTakeoverContestedEvent($takeover, $request->user()));

        return $takeover;
    }
}
