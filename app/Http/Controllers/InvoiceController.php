<?php

namespace App\Http\Controllers;

use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\Invoice\CreateRequest;
use App\Models\Invoice;
use App\Repositories\InvoiceRepository;

class InvoiceController extends RestController
{
    public function __construct(InvoiceRepository $repository, Invoice $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(CreateRequest $request)
    {
        $item = parent::validateAndCreate($request);

        if ($request->get("apply_to_balance")) {
            $user = $item->user;
            $total = $item->total;

            $user->updateBalance($total);
        }

        return $this->respondWithItem($request, $item, 201);
    }

    public function update(Request $request, $id)
    {
        $item = parent::validateAndUpdate($request, $id);

        return $this->respondWithItem($request, $item);
    }

    public function destroy(Request $request, $id)
    {
        return parent::validateAndDestroy($request, $id);
    }

    public function template()
    {
        return [
            "item" => [
                "apply_to_balance" => true,
                "bill_items" => [],
                "period" => "",
                "type" => "",
            ],
            "rules" => [],
        ];
    }
}
