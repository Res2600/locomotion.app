<?php

namespace App\Http\Controllers;

use App\Models\Community;
use App\Models\Loanable;
use App\Models\User;
use DB;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Storage;

class StaticController extends Controller
{
    public function redirectToSolon()
    {
        return redirect("https://solon-collectif.org/locomotion/");
    }

    public function blank()
    {
        return response("", 204);
    }

    public function notFound()
    {
        return abort(404);
    }

    public function status()
    {
        return view("status", [
            "database" => DB::statement("SELECT 1") ? "OK" : "Erreur",
        ]);
    }

    public function stats()
    {
        $communityQuery = Community::whereIn("type", ["borough", "private"]);

        $communityQuery->select("*")->withCustomColumn("center");

        return response(
            [
                "communities" => $communityQuery->get()->map(function ($c) {
                    return [
                        "id" => $c->id,
                        "name" => $c->name,
                        "center" => $c->center,
                        "area" => $c->area,
                        "center_google" => $c->center_google,
                        "type" => $c->type,
                        "description" => $c->description,
                    ];
                }),
                // User is not super admin
                "users" => User::whereRole(null)
                    // User is approved in at least one community
                    ->whereHas("approvedCommunities")
                    ->count(),
                "loanables" => Loanable::count(),
            ],
            200
        );
    }

    public function storage($path)
    {
        try {
            $file = Storage::disk(env("FILESYSTEM_DRIVER"))->get(
                "/storage/" . $path
            );
        } catch (FileNotFoundException) {
            return null;
        }
        preg_match("/.*\.(.*)/", $path, $out);
        $mimeType = str_ends_with($path, "pdf")
            ? "application/pdf"
            : "image/" . $out[1];
        return (new Response($file, 200))->header("Content-Type", $mimeType);
    }

    public function app()
    {
        return view("app");
    }
}
