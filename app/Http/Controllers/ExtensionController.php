<?php

namespace App\Http\Controllers;

use App\Events\LoanExtensionAcceptedEvent;
use App\Events\LoanExtensionCreatedEvent;
use App\Events\LoanExtensionRejectedEvent;
use App\Http\ErrorResponse;
use App\Http\Requests\Action\ExtensionRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Extension;
use App\Repositories\ExtensionRepository;
use Gate;
use Illuminate\Validation\ValidationException;

class ExtensionController extends RestController
{
    public function __construct(
        ExtensionRepository $repository,
        Extension $model
    ) {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(ExtensionRequest $request)
    {
        Gate::authorize("declareExtension", $request->loan);

        $extension = parent::validateAndCreate($request);

        event(new LoanExtensionCreatedEvent($extension));

        return $this->respondWithItem($request, $extension, 201);
    }

    public function complete(ExtensionRequest $request, $loanId, $extensionId)
    {
        /** @var Extension $extension */
        $extension = $this->repo->find($request, $extensionId);
        Gate::authorize("acceptExtension", $extension->loan);

        $extension->message_for_borrower = $request->get(
            "message_for_borrower"
        );
        $extension->complete();
        $extension->save();

        event(new LoanExtensionAcceptedEvent($extension));

        return $extension;
    }

    public function cancel(Request $request, $loanId, $extensionId)
    {
        /** @var Extension $extension */
        $extension = $this->repo->find($request, $extensionId);
        Gate::authorize("cancelExtension", $extension->loan);

        $extension->message_for_borrower = $request->get(
            "message_for_borrower"
        );

        $extension->cancel();
        $extension->save();

        return $extension;
    }

    // function to reject the extension requested
    public function reject(Request $request, $loanId, $extensionId)
    {
        /** @var Extension $extension */
        $extension = $this->repo->find($request, $extensionId);
        Gate::authorize("rejectExtension", $extension->loan);

        $extension->message_for_borrower = $request->get(
            "message_for_borrower"
        );
        $extension->reject();
        $extension->save();

        event(new LoanExtensionRejectedEvent($extension));

        return $extension;
    }
}
