<?php

namespace App\Http\Controllers;

use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\Padlock\IndexRequest;
use App\Http\Requests\Padlock\RestoreRequest;
use App\Http\Requests\Padlock\RetrieveRequest;
use App\Http\Requests\Padlock\UpdateRequest;
use App\Models\Padlock;
use App\Repositories\PadlockRepository;

class PadlockController extends RestController
{
    public function __construct(PadlockRepository $repository, Padlock $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function update(UpdateRequest $request, $id)
    {
        $item = parent::validateAndUpdate($request, $id);

        return $this->respondWithItem($request, $item);
    }

    public function restore(RestoreRequest $request, $id)
    {
        return parent::validateAndRestore($request, $id);
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "name" => "",
                "mac_address" => "",
                "external_id" => "",
                "loanable_id" => null,
            ],
            "form" => [
                "name" => [
                    "type" => "text",
                ],
                "mac_address" => [
                    "type" => "text",
                ],
                "external_id" => [
                    "type" => "text",
                ],
                "loanable_id" => [
                    "type" => "relation",
                    "query" => [
                        "slug" => "loanables",
                        "value" => "id",
                        "text" => "name",
                        "params" => [
                            "fields" => "id,name",
                            "!type" => "car",
                        ],
                    ],
                ],
            ],
        ];

        $modelRules = $this->model->getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            $template["form"][$field]["rules"] = $this->formatRules($rules);
        }

        return $template;
    }
}
