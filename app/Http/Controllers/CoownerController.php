<?php

namespace App\Http\Controllers;

use App\Events\CoownerAddedEvent;
use App\Events\CoownerRemovedEvent;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\Loanable\CreateCoownerRequest;
use App\Models\Coowner;
use App\Models\Loanable;
use App\Repositories\CoownerRepository;
use Gate;
use Request;

class CoownerController extends RestController
{
    public function __construct(CoownerRepository $repository, Coowner $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(CreateCoownerRequest $request, $loanableId)
    {
        $loanable = Loanable::findOrFail($loanableId);
        Gate::authorize("addCoowner", $loanable);
        $coowner = $loanable->addCoowner($request->get("user_id"));

        event(new CoownerAddedEvent($request->user(), $loanable, $coowner));

        return $coowner;
    }

    public function delete(BaseRequest $request, $coownerId)
    {
        $coowner = Coowner::findOrFail($coownerId);
        Gate::authorize("delete", $coowner);
        $coowner->delete();

        event(new CoownerRemovedEvent($coowner, $request->user()));
    }

    public function update(BaseRequest $request, $coownerId)
    {
        $coowner = Coowner::findOrFail($coownerId);
        Gate::authorize("update", $coowner);
        return $this::validateAndUpdate($request, $coownerId);
    }
}
