<?php

namespace App\Http\Controllers;

use App\Events\LoanPaidEvent;
use App\Http\Requests\BaseRequest;
use App\Models\Invoice;
use App\Models\Loan;
use App\Models\Payment;
use App\Repositories\LoanRepository;
use App\Repositories\PaymentRepository;
use Carbon\Carbon;
use Gate;

class PaymentController extends RestController
{
    public function __construct(LoanRepository $repository, Payment $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function complete(BaseRequest $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);

        Gate::authorize("pay", $loan);
        $this->pay($loan);

        return $loan->payment;
    }

    /**
     * Creates the invoices and transfers the amounts from user's balance for the completion of this loan.
     *
     * @param bool $isAutomated Whether this is an automated payment or triggered by user action
     */
    public static function pay(Loan $loan, bool $isAutomated = false): void
    {
        // Prepare variables
        $price = $loan->actual_price;
        $insurance = $loan->actual_insurance;
        $platformTip = $loan->platform_tip;
        $expenses = $loan->handover->purchases_amount;
        $object = $loan->loanable->name;
        $prettyDate = (new Carbon($loan->departure_at))
            ->locale("fr_FR")
            ->isoFormat("LLL");

        // Update loan
        $loan->final_price = $price;
        $loan->final_insurance = $insurance;
        $loan->final_platform_tip = $platformTip;
        $loan->final_purchases_amount = $expenses;
        $loan->save();

        // Build line items
        $items = [
            "price" => [
                "label" => "Coût de l'emprunt de $object le $prettyDate",
                "amount" => $price,
                "item_date" => date("Y-m-d"),
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "amount_type" => null,
            ],
            "insurance" => $insurance
                ? [
                    "label" => "Coût de l'assurance pour l'emprunt de $object le $prettyDate",
                    "amount" => $insurance,
                    "item_date" => date("Y-m-d"),
                    "taxes_tps" => 0,
                    "taxes_tvq" => 0,
                    "amount_type" => null,
                ]
                : null,
            "expenses" => $expenses
                ? [
                    "label" => "Dépenses pour l'emprunt de $object le $prettyDate",
                    "amount" => -$expenses,
                    "item_date" => date("Y-m-d"),
                    "taxes_tps" => 0,
                    "taxes_tvq" => 0,
                    "amount_type" => null,
                ]
                : null,
            "platform_tip" => $platformTip
                ? [
                    "label" => "Contribution volontaire pour l'emprunt de $object le $prettyDate",
                    "amount" => round($platformTip / 1.14975, 2),
                    "item_date" => date("Y-m-d"),
                    "taxes_tps" => round(($platformTip / 1.14975) * 0.05, 2),
                    "taxes_tvq" => round(($platformTip / 1.14975) * 0.09975, 2),
                    "amount_type" => null,
                ]
                : null,
        ];

        // Update invoices
        $borrowerUser = $loan->borrower->user;

        // Create an invoice as a debit, since it is a payment
        $borrowerInvoice = $borrowerUser->createInvoice("debit");
        foreach ($items as $key => $item) {
            if ($item) {
                // Determine whether the item is a debit or credit
                if ($key != "expenses") {
                    $item["amount_type"] = "debit";
                } else {
                    $item["amount_type"] = "credit";
                }

                // Create a bill item in the invoice
                $borrowerInvoice->billItems()->create($item);
            }
        }
        $borrowerInvoice->pay();

        $ownerUser = $loan->loanable->owner->user;
        // Create an invoice as a credit, since the owner will
        // receive this amount from the loan
        $ownerInvoice = $ownerUser->createInvoice("credit");

        if ($items["price"]) {
            // Cost of the loan will be received by the owner
            $items["price"]["amount_type"] = "credit";

            $ownerInvoice->billItems()->create($items["price"]);
        }

        if ($items["expenses"]) {
            // Expenses are deducted from the amount received by the owner
            $items["expenses"]["amount_type"] = "debit";

            $ownerInvoice->billItems()->create($items["expenses"]);
        }

        $ownerInvoice->pay();

        $debitAmount = $price + $insurance + $platformTip - $expenses;
        $creditAmount = $price - $expenses;

        // Update balances
        if ($borrowerUser->is($ownerUser)) {
            // If the borrower is the owner we do a single atomic addToBalance
            // or removeFromBalance instead of both calls, so we can allow
            // temporarily going below a balance of zero if the final balance
            // is above zero (e.g. initial balance is 0.5 => debit 1 => balance
            // is -0.5 => credit 1 ==> final balance is 0.5)
            $movement = $creditAmount - $debitAmount;

            if ($movement >= 0) {
                $ownerUser->addToBalance($movement);
            } else {
                $ownerUser->removeFromBalance($movement * -1);
            }
        } else {
            $borrowerUser->removeFromBalance($debitAmount);
            $ownerUser->refresh();
            $ownerUser->addToBalance($creditAmount);
        }

        // Save payment
        // Borrower invoice is always created.
        $payment = $loan->payment;
        $payment->borrower_invoice_id = $borrowerInvoice->id;
        $payment->owner_invoice_id = $ownerInvoice->id;
        $payment->complete();
        $payment->save();

        // Send emails after an automated or manual action
        if (
            !$loan->loanable->is_self_service &&
            $loan->loanable->owner &&
            $loan->total_final_cost > 0
        ) {
            if ($isAutomated) {
                event(
                    new LoanPaidEvent(
                        $borrowerUser,
                        $borrowerInvoice->getTransformer()->transform([
                            "fields" => ["*" => "*"],
                        ]),
                        "Conclusion automatique de votre emprunt",
                        "Votre emprunt est terminé depuis 48h. Il est désormais clôturé!"
                    )
                );

                // Trigger event for owner if exists and is not also the borrower.
                if ($loan->loanable->owner && !$borrowerUser->is($ownerUser)) {
                    event(
                        new LoanPaidEvent(
                            $ownerUser,
                            $ownerInvoice->getTransformer()->transform([
                                "fields" => ["*" => "*"],
                            ]),
                            null,
                            "Votre emprunt est désormais clôturé!"
                        )
                    );
                }
            } else {
                event(
                    new LoanPaidEvent(
                        $borrowerUser,
                        $borrowerInvoice->getTransformer()->transform([
                            "fields" => ["*" => "*"],
                        ])
                    )
                );

                // Trigger event for owner if exists and is not also the borrower.
                if ($loan->loanable->owner && !$borrowerUser->is($ownerUser)) {
                    event(
                        new LoanPaidEvent(
                            $ownerUser,
                            $ownerInvoice->getTransformer()->transform([
                                "fields" => ["*" => "*"],
                            ])
                        )
                    );
                }
            }
        }
    }
}
