<?php

namespace App\Http\Controllers;

use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Borrower;
use App\Repositories\BorrowerRepository;

class BorrowerController extends RestController
{
    protected $canReturnCsv = false;

    public function __construct(BorrowerRepository $repo, Borrower $model)
    {
        $this->repo = $repo;
        $this->model = $model;
    }
}
