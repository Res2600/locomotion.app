<?php

namespace App\Http\Controllers;

use App\Events\LoanIntentionAcceptedEvent;
use App\Events\LoanIntentionRejectedEvent;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Intention;
use App\Models\Loan;
use App\Repositories\IntentionRepository;
use App\Repositories\LoanRepository;
use Gate;

class IntentionController extends RestController
{
    public function __construct(LoanRepository $repository, Intention $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function complete(Request $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);

        Gate::authorize("accept", $loan);

        $intention = $loan->intention;
        $intention->message_for_borrower = $request->get(
            "message_for_borrower"
        );

        $intention->complete();
        $intention->save();
        $intention->refresh();

        // Move forward if possible.
        LoanController::loanActionsForward($intention->loan);

        event(new LoanIntentionAcceptedEvent($intention));

        return $intention;
    }

    public function cancel(Request $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);

        Gate::authorize("reject", $loan);

        $intention = $loan->intention;
        $intention->message_for_borrower = $request->get(
            "message_for_borrower"
        );

        $intention->cancel();
        $intention->save();

        event(new LoanIntentionRejectedEvent($intention));

        return $intention;
    }
}
