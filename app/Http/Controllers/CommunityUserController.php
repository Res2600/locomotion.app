<?php

namespace App\Http\Controllers;

use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest;
use App\Models\Pivots\CommunityUser;
use App\Repositories\RestRepository;
use Illuminate\Validation\ValidationException;

class CommunityUserController extends RestController
{
    public function __construct(CommunityUser $model)
    {
        $this->repo = new RestRepository($model);
        $this->model = $model;
    }

    public function updateCommunityUser(
        BaseRequest $request,
        int $communityUserId
    ) {
        return $this->repo->update(
            $request,
            $communityUserId,
            $request->json()->all()
        );
    }
}
