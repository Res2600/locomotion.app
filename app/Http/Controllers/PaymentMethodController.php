<?php

namespace App\Http\Controllers;

use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\PaymentMethod\CreateRequest;
use App\Models\PaymentMethod;
use App\Repositories\PaymentMethodRepository;
use Auth;
use Illuminate\Validation\ValidationException;
use Stripe;

class PaymentMethodController extends RestController
{
    protected $canReturnCsv = false;

    public function __construct(
        PaymentMethodRepository $repository,
        PaymentMethod $model
    ) {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(CreateRequest $request)
    {
        $user = $request->user();

        $customer = $user->getStripeCustomer();
        $card = Stripe::createCardBySourceId(
            $customer->id,
            $request->get("external_id")
        );

        $request->merge([
            "external_id" => $card->id,
            "country" => $card->country,
        ]);

        if (!$request->get("user_id")) {
            $request->merge(["user_id" => $request->user()->id]);
        }

        $item = parent::validateAndCreate($request);

        return $this->respondWithItem($request, $item, 201);
    }

    public function update(Request $request, $id)
    {
        $item = parent::validateAndUpdate($request, $id);

        return $this->respondWithItem($request, $item);
    }

    public function retrieve(Request $request, $id)
    {
        $item = $this->repo->find($request, $id);

        return $this->respondWithItem($request, $item);
    }

    public function destroy(Request $request, $id)
    {
        $item = $this->repo->find($request, $id);

        $user = $request->user();
        $customer = $user->getStripeCustomer();
        Stripe::deleteSource($customer->id, $item->external_id);

        return parent::validateAndDestroy($request, $id);
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "name" => "",
            ],
            "form" => [
                "name" => [
                    "type" => "text",
                ],
                "external_id" => [
                    "type" => "text",
                ],
                "four_last_digits" => [
                    "type" => "number",
                    "disabled" => true,
                ],
            ],
        ];

        $modelRules = $this->model->getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            if (!isset($template["form"][$field])) {
                continue;
            }
            $template["form"][$field]["rules"] = $this->formatRules($rules);
        }

        return $template;
    }
}
