<?php

namespace App\Http\Requests\Loanable;

use App\Http\Requests\BaseRequest;
use App\Models\Loanable;
use App\Models\User;

class CreateCoownerRequest extends BaseRequest
{
    public function rules(): array
    {
        $loanable = Loanable::findOrFail($this->route("loanable_id"));

        $usersSharingCommunity = User::approvedInSharedCommunities(
            $loanable->owner->user->id
        )
            ->pluck("id")
            ->join(",");

        return [
            "user_id" => ["required", "in:$usersSharingCommunity"],
        ];
    }
}
