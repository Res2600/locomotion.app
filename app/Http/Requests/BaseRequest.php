<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class BaseRequest extends FormRequest
{
    protected static function rebaseRules($scope, $rules)
    {
        return array_reduce(
            array_keys($rules),
            function ($acc, $key) use ($rules, $scope) {
                $acc[$scope . "." . $key] = $rules[$key];
                return $acc;
            },
            []
        );
    }

    private $fieldsMemo = null;
    private $notFieldsMemo = null;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }

    public function getFields()
    {
        if ($this->fieldsMemo !== null) {
            return $this->fieldsMemo;
        }

        if (!$this->get("fields")) {
            return $this->fieldsMemo = ["*" => "*"];
        }

        return $this->fieldsMemo = ParseFieldsHelper::parseFields(
            ParseFieldsHelper::splitFields($this->get("fields"))
        );
    }

    public function getNotFields()
    {
        if ($this->notFieldsMemo !== null) {
            return $this->notFieldsMemo;
        }

        if (!$this->get("!fields")) {
            return $this->notFieldsMemo = [];
        }

        return $this->notFieldsMemo = ParseFieldsHelper::parseFields(
            ParseFieldsHelper::splitFields($this->get("!fields"))
        );
    }

    public function redirectAs($user, $requestClass = null)
    {
        $class = $requestClass ? $requestClass : static::class;
        $newRequest = new $class();
        $newRequest->setUserResolver(function () use ($user) {
            return $user;
        });
        if (!$newRequest->authorize()) {
            return abort(403);
        }
        return $newRequest;
    }

    public function redirectAuth($requestClass = null)
    {
        $class = $requestClass ? $requestClass : static::class;
        $newRequest = new $class();
        $newRequest->setUserResolver(function () {
            return $this->user();
        });
        if (!$newRequest->authorize()) {
            return abort(403);
        }
        return $newRequest;
    }

    public function redirect($requestClass)
    {
        $newRequest = new $requestClass();
        $newRequest->setUserResolver(function () {
            return $this->user();
        });
        $newRequest->setRouteResolver(function () {
            return $this->route();
        });
        $newRequest->merge($this->all());
        $newRequest->setJson($this->json());

        if (!$newRequest->authorize()) {
            return abort(403);
        }

        $validator = Validator::make(
            $newRequest->all(),
            $newRequest->rules(),
            $newRequest->messages(),
            $newRequest->attributes()
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $newRequest;
    }
}
