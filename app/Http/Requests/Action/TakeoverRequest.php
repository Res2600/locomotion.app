<?php

namespace App\Http\Requests\Action;

use App\Enums\LoanableTypes;
use App\Http\Requests\BaseRequest;
use App\Models\Loan;

class TakeoverRequest extends BaseRequest
{
    public function rules()
    {
        $loanId = $this->route("loan_id") ?: $this->get("loan_id");
        $loan = Loan::accessibleBy($this->user())->find($loanId);

        if ($loan->loanable->type === LoanableTypes::Car) {
            return [
                "mileage_beginning" => ["required", "integer"],
            ];
        }

        return [];
    }
}
