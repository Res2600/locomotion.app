<?php

namespace App\Http\Requests\Action;

use App\Http\Requests\BaseRequest;
use App\Models\Loan;
use App\Repositories\LoanRepository;

class ExtensionRequest extends BaseRequest
{
    /**
     * @var Loan loan
     */
    public $loan;

    public function rules()
    {
        $loan = $this->fetchLoan();

        $min =
            max(
                $loan->duration_in_minutes,
                $loan
                    ->extensions()
                    ->where("status", "=", "completed")
                    ->max("new_duration")
            ) + 15;

        return [
            "new_duration" => ["min:$min", "numeric"],
        ];
    }

    private function fetchLoan()
    {
        if ($this->loan) {
            return $this->loan;
        }

        $loanRepository = new LoanRepository(new Loan());
        $this->loan = $loanRepository->find(
            $this->redirectAuth(BaseRequest::class),
            $this->route("loan_id")
        );

        return $this->loan;
    }
}
