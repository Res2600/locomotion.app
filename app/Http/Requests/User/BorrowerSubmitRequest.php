<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use App\Models\Borrower;

class BorrowerSubmitRequest extends BaseRequest
{
    public function authorize()
    {
        return $this->user()->isAdmin() ||
            $this->user()->id === (int) $this->route("user_id");
    }

    public function rules()
    {
        return Borrower::getRules("submit");
    }
}
