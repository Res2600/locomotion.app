<?php

namespace App\Http\Requests\Community;

use App\Http\Requests\BaseRequest;
use App\Models\Community;
use App\Models\Pricing;
use App\Rules\CommunityPricingRule;

class UpdateRequest extends BaseRequest
{
    public function authorize()
    {
        $id = $this->route("id");
        return $this->user()->isAdmin() ||
            $this->user()->isAdminOfCommunity($id);
    }

    public function rules()
    {
        $rules = [
            "pricings" => [new CommunityPricingRule()],
        ];

        $pricingRules = Pricing::getRules("update", $this->user());
        return array_merge(
            $rules,
            static::rebaseRules("pricings.*", $pricingRules)
        );
    }
}
