<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Takeover */
class TakeoverResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "executed_at" => $this->executed_at,
            "status" => $this->status,
            "comments_on_vehicle" => $this->comments_on_vehicle,
            "contested_at" => $this->contested_at,
            "comments_on_contestation" => $this->comments_on_contestation,
            "mileage_beginning" => $this->mileage_beginning,

            "image" => new ImageResource($this->image),
        ];
    }
}
