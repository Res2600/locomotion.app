<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use RectorPrefix202212\Symfony\Component\Config\Resource\FileResource;

/** @mixin \App\Models\Handover */
class HandoverResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "executed_at" => $this->executed_at,
            "status" => $this->status,
            "comments_by_borrower" => $this->comments_by_borrower,
            "comments_by_owner" => $this->comments_by_owner,
            "purchases_amount" => $this->purchases_amount,
            "contested_at" => $this->contested_at,
            "comments_on_contestation" => $this->comments_on_contestation,
            "mileage_end" => $this->mileage_end,

            "image" => new ImageResource($this->image),
            "expense" => new ImageResource($this->expense),
        ];
    }
}
