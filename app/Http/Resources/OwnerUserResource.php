<?php

namespace App\Http\Resources;

use App\Models\Owner;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Owner */
class OwnerUserResource extends JsonResource
{
    public function __construct(
        Owner $owner,
        private readonly bool $showUserPhone = false
    ) {
        parent::__construct($owner);
    }
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => new UserBriefResource($this->user, $this->showUserPhone),
        ];
    }
}
