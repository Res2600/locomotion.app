<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Incident */
class IncidentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "executed_at" => $this->executed_at,
            "status" => $this->status,
            "comments_on_incident" => $this->comments_on_incident,
            "incident_type" => $this->incident_type,
        ];
    }
}
