<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Car */
class CarResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "brand" => $this->brand,
            "model" => $this->model,
            "year_of_circulation" => $this->year_of_circulation,
            "transmission_mode" => $this->transmission_mode,
            "engine" => $this->engine,
            "papers_location" => $this->papers_location,

            "report" => new FileResource($this->whenLoaded("report")),
        ];
    }
}
