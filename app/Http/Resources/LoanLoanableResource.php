<?php

namespace App\Http\Resources;

use App\Enums\LoanableTypes;
use App\Models\Loan;
use App\Models\Loanable;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Loanable */
class LoanLoanableResource extends JsonResource
{
    public function __construct(Loanable $loanable, private readonly Loan $loan)
    {
        parent::__construct($loanable);
    }

    public function toArray($request)
    {
        if ($this->trashed()) {
            return (new ArchivedLoanableResource($this))->toArray($request);
        }

        if ($this->type === LoanableTypes::Car) {
            $this->details->load("report");
        }

        return [
            "id" => $this->id,
            "type" => $this->type,
            "show_owner_as_contact" => $this->show_owner_as_contact,
            "position_google" => $this->position_google,
            "position" => $this->position,
            "comments" => $this->comments,
            "instructions" => $this->when(
                $this->loan->intention?->status === "completed",
                $this->instructions
            ),
            "is_self_service" => $this->is_self_service,
            "location_description" => $this->location_description,
            "name" => $this->name,
            "has_padlock" => $this->has_padlock,

            "owner" => new OwnerUserResource(
                $this->owner->load("user.avatar"),
                $this->show_owner_as_contact
            ),
            "coowners" => LoanCoownerResource::collection(
                $this->coowners->load("user.avatar")
            ),
            "image" => new ImageResource($this->image),
            "details" => $this->getLoanableDetailsResource($this->details),
            "padlock" => new PadlockResource($this->padlock),
        ];
    }

    private function getLoanableDetailsResource(
        $data
    ): TrailerResource|BikeResource|CarResource {
        return match ($this->type) {
            LoanableTypes::Bike => new BikeResource($data),
            LoanableTypes::Trailer => new TrailerResource($data),
            LoanableTypes::Car => new CarResource($data),
        };
    }
}
