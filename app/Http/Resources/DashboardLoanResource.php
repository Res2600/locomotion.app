<?php

namespace App\Http\Resources;

use App\Models\Loan;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loan */
class DashboardLoanResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "actual_return_at" => $this->actual_return_at,
            "borrower_validated_at" => $this->borrower_validated_at,
            "community" => new IdOnlyResource($this->community),
            "departure_at" => $this->departure_at,
            "duration_in_minutes" => $this->duration_in_minutes,
            "final_price" => $this->final_price,
            "final_purchases_amount" => $this->final_purchases_amount,
            "is_free" => $this->is_free,
            "needs_validation" => $this->needs_validation,
            "owner_validated_at" => $this->owner_validated_at,
            "status" => $this->status,
            "total_final_cost" => $this->total_final_cost,

            "extensions" => ActionStatusResource::collection($this->extensions),
            "handover" => new ActionStatusResource($this->handover),
            "incidents" => ActionStatusResource::collection($this->incidents),
            "intention" => new ActionStatusResource($this->intention),
            "payment" => new ActionStatusResource($this->payment),
            "pre_payment" => new ActionStatusResource($this->prePayment),
            "takeover" => new ActionStatusResource($this->takeover),

            "borrower" => new BorrowerUserResource($this->borrower),
            "loanable" => new DashboardLoanableResource($this->loanable),
        ];
    }
}
