<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Trailer */
class TrailerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "maximum_charge" => $this->maximum_charge,
            "dimensions" => $this->dimensions,
        ];
    }
}
