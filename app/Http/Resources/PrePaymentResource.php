<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\PrePayment */
class PrePaymentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "executed_at" => $this->executed_at,
            "status" => $this->status,
        ];
    }
}
