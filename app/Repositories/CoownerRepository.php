<?php

namespace App\Repositories;

use App\Models\Coowner;

class CoownerRepository extends RestRepository
{
    public function __construct(Coowner $model)
    {
        $this->model = $model;
    }
}
