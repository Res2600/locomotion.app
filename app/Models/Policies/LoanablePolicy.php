<?php

namespace App\Models\Policies;

use App\Models\Loanable;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanablePolicy
{
    use HandlesAuthorization;

    private static function isAdminOfOwnerCommunity(
        User $user,
        Loanable $loanable
    ): bool {
        return $loanable->owner &&
            $user->isAdminOfCommunityFor($loanable->owner->user->id);
    }

    public function viewInstructions(User $user, Loanable $loanable): bool
    {
        return $loanable->isOwnerOrCoowner($user) ||
            $user->isAdmin() ||
            self::isAdminOfOwnerCommunity($user, $loanable);
    }

    public function addCoowner(User $user, Loanable $loanable): bool
    {
        return $loanable->isOwner($user) || $user->isAdmin();
    }

    public function delete(User $user, Loanable $loanable): bool
    {
        return $loanable->isOwner($user) ||
            $user->isAdmin() ||
            $user->isAdminOfCommunityFor($loanable->owner->user->id);
    }

    public function restore(User $user, Loanable $loanable): bool
    {
        return $this->delete($user, $loanable);
    }

    public function update(User $user, Loanable $loanable): bool
    {
        return $loanable->isOwnerOrCoowner($user) ||
            self::isAdminOfOwnerCommunity($user, $loanable) ||
            $user->isAdmin();
    }

    public function updateOwner(User $user, Loanable $loanable)
    {
        if (
            !$user->isAdmin() &&
            !self::isAdminOfOwnerCommunity($user, $loanable)
        ) {
            return $this->deny("Only admins can change owners.");
        }
        return true;
    }

    public function updatePadlock(User $user, Loanable $loanable)
    {
        if (!$user->isAdmin()) {
            return $this->deny("Only global admins can update padlocks.");
        }
        return true;
    }

    public function listArchived(User $user)
    {
        return $user->isAdmin() || $user->isCommunityAdmin();
    }
}
