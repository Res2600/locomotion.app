<?php

namespace App\Models\Policies;

use App\Models\Coowner;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoownerPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, Coowner $coowner): bool
    {
        return $coowner->loanable->isOwner($user) ||
            $coowner->user->is($user) ||
            $user->isAdmin();
    }

    public function update(User $user, Coowner $coowner): bool
    {
        return $coowner->loanable->isOwner($user) ||
            $coowner->user->is($user) ||
            $user->isAdmin();
    }
}
