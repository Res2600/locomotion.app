<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Trailer extends LoanableDetails
{
    use HasFactory;

    protected $fillable = ["maximum_charge", "dimensions"];
}
