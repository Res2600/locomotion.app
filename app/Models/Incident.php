<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Incident extends Action
{
    use HasFactory;
    public static $rules = [
        "incident_type" => ["required", "in:accident,small_incident,general"],
        "comments_on_incident" => ["required"],
    ];

    protected $fillable = ["loan_id", "incident_type", "comments_on_incident"];

    public static function boot()
    {
        parent::boot();

        self::saved(function ($model) {
            if (!$model->executed_at) {
                switch ($model->status) {
                    case "completed":
                        $loanId = $model->loan->id;

                        $model->executed_at = Carbon::now();
                        $model->save();
                        break;
                    case "canceled":
                        $model->executed_at = Carbon::now();
                        $model->save();
                        break;
                }
            }
        });
    }

    protected static array $customColumns = [
        "type" => "'incident'",
    ];

    public $readOnly = false;

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }
}
