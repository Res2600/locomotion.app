<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Coowner extends BaseModel
{
    use HasFactory;

    public static $rules = [
        "title" => "nullable|string",
        "show_as_contact" => "boolean",
    ];

    public $fillable = ["title", "show_as_contact"];
    public $items = ["user", "loanable"];

    /**
     * We include archived users here, so we can get some information from co-owners when fetching
     * loanables. The @see ArchivedUserResource should be used to strip any personal information
     * that shouldn't be exposed.
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function loanable()
    {
        return $this->belongsTo(Loanable::class);
    }
}
