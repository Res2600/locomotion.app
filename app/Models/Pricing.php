<?php

namespace App\Models;

use App\Casts\ObjectTypeCast;
use App\Rules\PricingRule;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;

class Pricing extends BaseModel
{
    use HasFactory;
    public static $language;

    /**
     * @return null|array|float
     *     null if line is invalid
     *     array of two floats for [trip_cost, insurance_cost]
     *     float for trip_cost only
     */
    public static function evaluateRuleLine($line, $data)
    {
        $language = static::getExpressionLanguage();

        // Skip:
        // - comments (lines starting with # preceded by any whitespace).
        // - empty lines or containing whitespace only
        if (preg_match("/^\s*#/", $line) || preg_match('/^\s*$/', $line)) {
            return null;
        }

        if (preg_match('/^SI\s+.+?\s+ALORS\s+.+$/', $line)) {
            $line = str_replace("SI", "", $line);
            $line = str_replace("ALORS", "?", $line);
            $line .= ": null";
        }

        $line = str_replace('$KM', "km", $line);
        $line = str_replace('$MINUTES', "minutes", $line);
        $line = str_replace('$OBJET', "loanable", $line);
        $line = str_replace('$EMPRUNT', "loan", $line);
        $line = str_replace(
            '$PRIME_PAR_JOUR',
            "loanable.daily_premium_from_value_category",
            $line
        );
        $line = str_replace(
            '$SURCOUT_ASSURANCE',
            "(loanable.type == 'car' " .
                "and (loan.start.year_eight_months_ago - loanable.year_of_circulation) <= 5)",
            $line
        );

        $line = str_replace(" NON ", " !", $line);
        $line = str_replace(" OU ", " or ", $line);
        $line = str_replace(" ET ", " and ", $line);

        $line = str_replace(" PAS DANS ", " not in ", $line);
        $line = str_replace(" DANS ", " in ", $line);

        return $language->evaluate($line, $data);
    }

    public static function getExpressionLanguage()
    {
        if (self::$language) {
            return self::$language;
        }

        $language = new ExpressionLanguage();
        $language->register(
            "MIN",
            function ($a, $b) {
                return sprintf(
                    '(is_numeric(%1$s) && is_numeric(%2$s) ? min(%1$s, %2$s) : null)',
                    $a,
                    $b
                );
            },
            function ($arguments, $a, $b) {
                if (!is_numeric($a) || !is_numeric($b)) {
                    return null;
                }

                return min($a, $b);
            }
        );
        $language->register(
            "MAX",
            function ($a, $b) {
                return sprintf(
                    '(is_numeric(%1$s) && is_numeric(%2$s) ? max(%1$s, %2$s) : null)',
                    $a,
                    $b
                );
            },
            function ($arguments, $a, $b) {
                if (!is_numeric($a) || !is_numeric($b)) {
                    return null;
                }

                return max($a, $b);
            }
        );
        $language->register(
            "PLANCHER",
            function ($str) {
                return sprintf(
                    '(is_numeric(%1$s) ? intval(floor(%1$s)) : %1$s)',
                    $str
                );
            },
            function ($arguments, $str) {
                if (!is_numeric($str)) {
                    return $str;
                }

                return intval(floor($str));
            }
        );
        $language->register(
            "PLAFOND",
            function ($str) {
                return sprintf(
                    '(is_numeric(%1$s) ? intval(ceil(%1$s)) : %1$s)',
                    $str
                );
            },
            function ($arguments, $str) {
                if (!is_numeric($str)) {
                    return $str;
                }

                return intval(ceil($str));
            }
        );
        $language->register(
            "ARRONDI",
            function ($str) {
                return sprintf(
                    '(is_numeric(%1$s) ? intval(round(%1$s)) : %1$s)',
                    $str
                );
            },
            function ($arguments, $str) {
                if (!is_numeric($str)) {
                    return $str;
                }

                return intval(round($str));
            }
        );
        $language->register(
            "DOLLARS",
            function ($str) {
                return sprintf(
                    '(is_numeric(%1$s) ? intval(round(%1$s), 2) : %1$s)',
                    $str
                );
            },
            function ($arguments, $str) {
                if (!is_numeric($str)) {
                    return $str;
                }

                return number_format(round($str, 2), 2);
            }
        );

        self::$language = $language;

        return self::$language;
    }

    public static function dateToDataObject($date)
    {
        $date = new Carbon($date);
        return (object) [
            "year" => $date->year,
            "month" => $date->month,
            "day" => $date->day,
            "hour" => $date->hour,
            "minute" => $date->minute,
            "day_of_year" => $date->dayOfYear,
            "year_eight_months_ago" => $date->copy()->sub(8, "months")->year,
        ];
    }

    public static $rules = [
        "name" => "required",
        "object_type" => ["nullable"],
        "rule" => ["required"],
    ];

    public static function getRules($action = "", $auth = null)
    {
        $rules = static::$rules;
        $rules["rule"][] = new PricingRule();
        return $rules;
    }

    protected $casts = [
        "object_type" => ObjectTypeCast::class,
    ];

    protected $fillable = ["name", "object_type", "rule"];

    public $items = ["community"];

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function evaluateRule(
        $km,
        $minutes,
        array|Loanable $loanable = [],
        $loan = null
    ) {
        $lines = explode("\n", $this->rule);

        if ($loanable instanceof Loanable) {
            $loanable->load("details");

            if ($loanable->type->value == "car") {
                $loanable->details->append("daily_premium_from_value_category");
            }

            $loanableData = $loanable->toArray();
        } else {
            $loanableData = $loanable;
        }

        if (key_exists("details", $loanableData) && $loanableData["details"]) {
            // Make detail properties accessible from the root
            $loanableData = [...$loanableData, ...$loanableData["details"]];
            // Remove details so it is not repeated
            unset($loanableData["details"]);
        }

        if ($loan instanceof Loan) {
            $start = new Carbon($loan->departure_at);
            $end = $start
                ->copy()
                ->add($loan->actual_duration_in_minutes, "minutes");

            $loanData = [
                "days" => $loan->calendar_days,
                "start" => self::dateToDataObject($start),
                "end" => self::dateToDataObject($end),
            ];
        } else {
            $loanData = $loan;
        }

        $roundCost = function ($currency) {
            return round($currency * 100.0) / 100;
        };

        $response = null;
        foreach ($lines as $line) {
            try {
                $response = static::evaluateRuleLine($line, [
                    "km" => $km,
                    "minutes" => $minutes,
                    "loanable" => (object) $loanableData,
                    "loan" => (object) $loanData,
                ]);

                // Skip lines that return null (comments, etc.)
                if ($response !== null) {
                    break;
                }
            } catch (SyntaxError) {
                // Should not happen but let it go at this point
            }
        }

        if (!is_array($response)) {
            return is_numeric($response)
                ? ["time" => $roundCost($response)]
                : null;
        }
        if (!isset($response["time"]) && isset($response[0])) {
            $response["time"] = $response[0];
            unset($response[0]);
        }
        if (!isset($response["insurance"]) && isset($response[1])) {
            $response["insurance"] = $response[1];
            unset($response[1]);
        }

        $allNumeric = true;
        foreach ($response as $value) {
            if (!is_numeric($value)) {
                $allNumeric = false;
            }
        }

        return $allNumeric ? array_map($roundCost, $response) : null;
    }
}
