<?php

namespace App\Models;

use App\Http\Resources\ArchivedUserResource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends BaseModel
{
    use SoftDeletes;

    use HasFactory;
    public static $rules = [
        "approved_at" => "nullable|date",
        "submitted_at" => "nullable|date",
    ];

    protected $fillable = ["approved_at", "submitted_at", "user_id"];

    public $collections = ["loanables", "cars", "bikes", "trailers"];

    public $items = ["user"];

    public $morphOnes = [
        "licence" => "imageable",
    ];

    /**
     * We include archived users here, so we can get some information from owners when fetching
     * loanables. The @see ArchivedUserResource should be used to strip any personal information
     * that shouldn't be exposed.
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function loanables()
    {
        return $this->hasMany(Loanable::class);
    }

    public function scopeSearch(Builder $query, $q)
    {
        return $query->whereHas("user", function ($q2) use ($q) {
            return $q2->search($q);
        });
    }
}
