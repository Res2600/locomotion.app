<?php

namespace App\Models;

use App\Exports\BaseExport;
use App\Transformers\Transformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOneOrMany;
use Illuminate\Database\Query\Expression;
use Nette\NotImplementedException;

trait BaseModelTrait
{
    public static $filterTypes = [];

    public static $export = BaseExport::class;

    protected static $transformer = Transformer::class;

    public function getTransformer(?Transformer $parent = null): Transformer
    {
        return new $this::$transformer($this, $parent);
    }

    protected static array $customColumns = [];
    protected static array $customJoins = [];

    public static function getRules($action = "", $auth = null)
    {
        return match ($action) {
            "destroy" => [],
            default => static::$rules,
        };
    }

    public static $rules = [];

    public static $validationMessages = [];

    public static function addJoin(
        $query,
        $table,
        $left,
        $operator = null,
        $right = null
    ) {
        if (!$query) {
            return $query;
        }

        if (isset($query->getQuery()->joins)) {
            $joins = $query->getQuery()->joins ?: [];
        } else {
            $joins = [];
        }

        $addJoin = true;
        foreach ($joins as $join) {
            if ($join->table === $table) {
                $addJoin = false;
                break;
            }
        }

        if ($addJoin) {
            if (is_callable($left)) {
                return $query->leftJoin($table, $left);
            }

            return $query->leftJoin($table, $left, $operator, $right);
        }

        return $query;
    }

    protected $appends = [];

    public $readOnly = false;

    public $items = [];

    public $collections = [];

    public $computed = [];

    public $morphOnes = [];

    public $morphManys = [];

    public function isItemRelation($fieldName): bool
    {
        return in_array($fieldName, $this->items) ||
            in_array($fieldName, array_keys($this->morphOnes));
    }

    public function isCollectionRelation($fieldName): bool
    {
        return in_array($fieldName, $this->collections) ||
            in_array($fieldName, array_keys($this->morphManys));
    }

    public function getWith()
    {
        return $this->with;
    }

    public function belongsTo(
        $related,
        $foreignKey = null,
        $ownerKey = null,
        $relation = null
    ) {
        if (is_null($relation)) {
            $relation = $this->guessBelongsToRelation();
        }

        $query = parent::belongsTo($related, $foreignKey, $ownerKey, $relation);

        return $query->withCustomColumns();
    }

    // FIXME Doesn't add the custom keys on the resulting object
    public function belongsToMany(
        $related,
        $table = null,
        $foreignPivotKey = null,
        $relatedPivotKey = null,
        $parentKey = null,
        $relatedKey = null,
        $relation = null
    ) {
        $instance = $this->newRelatedInstance($related);

        $foreignPivotKey = $foreignPivotKey ?: $this->getForeignKey();

        $relatedPivotKey = $relatedPivotKey ?: $instance->getForeignKey();

        if (is_null($table)) {
            $table = $this->joiningTable($related, $instance);
        }

        $query = $instance->newQuery()->withCustomColumns();

        return $this->newBelongsToMany(
            $query,
            $this,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey ?: $this->getKeyName(),
            $relatedKey ?: $instance->getKeyName(),
            $relation
        );
    }

    public function addRelationshipJoin($query, $relation, $tableAlias)
    {
        $rel = $this->{$relation}();
        $relatedModel = $rel->getRelated();
        $relatedTable = $relatedModel->getTable();

        $relatedAlias = "{$tableAlias}_{$relatedTable}";

        if ($rel instanceof BelongsTo) {
            $relatedKey = "$relatedAlias.{$rel->getOwnerKeyName()}";
            $localKey = "$tableAlias.{$rel->getForeignKeyName()}";
        } elseif ($rel instanceof HasOneOrMany) {
            $relatedKey = "$relatedAlias.{$rel->getForeignKeyName()}";
            $localKey = "$tableAlias.{$rel->getLocalKeyName()}";
        } else {
            throw new NotImplementedException();
        }

        $this::addJoin(
            $query,
            "$relatedTable as $relatedAlias",
            $relatedKey,
            "=",
            $localKey
        );

        return [$relatedAlias, $relatedModel];
    }

    public function addCustomJoins($query, $tableAlias)
    {
        foreach ($this::$customJoins as $relation => $aggregate) {
            $this->addRelationshipJoin($query, $relation, $tableAlias);
            if ($aggregate) {
                $query->groupBy("{$tableAlias}.id");
            }
        }

        return $query;
    }

    public function isCustomColumn($param)
    {
        return in_array($param, array_keys($this::$customColumns));
    }

    public function getCustomColumnSql($column, $tableAlias = null)
    {
        if (!$tableAlias) {
            $tableAlias = $this->getTable();
        }

        $sql = $this::$customColumns[$column];
        return str_replace("%table%", $tableAlias, $sql);
    }

    public function addCustomColumn($query, $columnName, $tableAlias = null)
    {
        $sql = $this->getCustomColumnSql($columnName, $tableAlias);
        return $query->selectRaw("{$sql} AS $columnName");
    }

    private function getQueryColumns($query)
    {
        $queryColumns = [];

        $from = $query->getQuery()->from;
        if ($from instanceof Expression) {
            // We have a subquery
            preg_match_all("/AS (\w+)/i", "$from", $matches);
            if ($matches && count($matches) > 1) {
                $queryColumns = $matches[1];
            }
        }

        return array_merge(
            $queryColumns,
            array_map(
                function ($c) {
                    return preg_replace("/.*AS /is", "", "$c");
                },
                isset($query->getQuery()->columns)
                    ? $query->getQuery()->columns
                    : []
            )
        );

        // Check sub requests
    }

    public function scopeWithCustomColumn($query, $columnName)
    {
        return $this->scopeWithCustomColumns($query, [$columnName]);
    }

    public function scopeWithCustomColumns(Builder $query, $columns = null)
    {
        if ($columns && !in_array("*", $columns)) {
            $customColumns = array_intersect_key(
                $this::$customColumns,
                array_flip($columns)
            );
        } else {
            $customColumns = $this::$customColumns;
        }

        // Avoid re-adding already selected columns.
        $queryColumns = $this->getQueryColumns($query);
        $columnsToAdd = array_diff_key(
            $customColumns,
            array_flip($queryColumns)
        );
        if (empty($columnsToAdd)) {
            return $query;
        }

        // if '*' has not been added yet for the table, we must add it.
        // Otherwise, only the custom columns will be present.
        $table = $this->getTable();
        $selectAllColumns = "$table.*";
        if (
            !in_array($selectAllColumns, $queryColumns) &&
            !in_array("*", $queryColumns)
        ) {
            $query->addSelect($selectAllColumns);
        }
        $this->addCustomJoins($query, $table);

        foreach ($columnsToAdd as $name => $column) {
            $query = $this->addCustomColumn($query, $name, $table);
        }

        return $query;
    }
}
