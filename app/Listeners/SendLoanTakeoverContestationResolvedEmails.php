<?php

namespace App\Listeners;

use App\Events\LoanTakeoverContestationResolvedEvent;
use App\Mail\Loan\TakeoverContestationResolved as LoanTakeoverContestationResolved;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanTakeoverContestationResolvedEmails
{
    /*
       Send contestation-resolved notification to owner and borrower.
       Only send one copy if owner is borrower.

       These rules apply for on-demand as well as self-service vehicles.
    */
    public function handle(LoanTakeoverContestationResolvedEvent $event)
    {
        $loan = $event->takeover->loan;
        $admin = $event->user;
        $borrower = $loan->borrower;

        UserMail::queue(
            new LoanTakeoverContestationResolved(
                $event->takeover,
                $loan,
                $borrower->user,
                $admin
            ),
            $borrower->user
        );

        $loan->loanable->queueMailToOwners(function (User $user) use (
            $event,
            $loan,
            $admin
        ) {
            return new LoanTakeoverContestationResolved(
                $event->takeover,
                $loan,
                $user,
                $admin
            );
        }, $borrower->user);
    }
}
