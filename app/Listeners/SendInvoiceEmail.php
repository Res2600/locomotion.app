<?php

namespace App\Listeners;

use App\Mail\InvoicePaid;
use App\Mail\UserMail;

class SendInvoiceEmail
{
    public function handle($event)
    {
        UserMail::queue(
            new InvoicePaid(
                $event->user,
                $event->invoice,
                $event->title,
                $event->text,
                $event->subject
            ),
            $event->user
        );
    }
}
