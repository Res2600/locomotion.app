<?php

namespace App\Listeners;

use App\Events\LoanableCreatedEvent;
use App\Mail\LoanableCreated;
use App\Mail\LoanableReviewable;
use App\Mail\UserMail;

class SendLoanableCreatedEmails
{
    public function handle(LoanableCreatedEvent $event)
    {
        UserMail::queue(
            new LoanableCreated($event->user, $event->loanable),
            $event->user
        );
        foreach ($event->user->communities as $community) {
            foreach ($community->admins() as $admin) {
                UserMail::queue(
                    new LoanableReviewable(
                        $event->user,
                        $community,
                        $event->loanable
                    ),
                    $admin
                );
            }
        }
    }
}
