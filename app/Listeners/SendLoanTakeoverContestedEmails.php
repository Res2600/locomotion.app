<?php

namespace App\Listeners;

use App\Events\LoanTakeoverContestedEvent;
use App\Mail\Loan\TakeoverContested as LoanTakeoverContested;
use App\Mail\Loan\TakeoverReviewable as LoanTakeoverReviewable;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanTakeoverContestedEmails
{
    /*
       Send loan-takeover-contested notification to:
         - owner if the borrower has contested
         - borrower if the owner has contested

       Also notify admins because they are the only ones who can resolve contestations.

       These rules apply for on-demand as well as self-service vehicles.
    */
    public function handle(LoanTakeoverContestedEvent $event)
    {
        $loan = $event->takeover->loan;
        $caller = $event->user;
        $borrower = $loan->borrower;

        if ($caller->id !== $borrower->user->id) {
            UserMail::queue(
                new LoanTakeoverContested(
                    $event->takeover,
                    $loan,
                    $borrower->user,
                    $caller
                ),
                $borrower->user
            );
        }

        $loan->loanable->queueMailToOwners(function (User $user) use (
            $event,
            $loan,
            $caller
        ) {
            return new LoanTakeoverContested(
                $event->takeover,
                $loan,
                $user,
                $caller
            );
        }, $caller);

        foreach ($loan->community->admins() as $admin) {
            UserMail::queue(
                new LoanTakeoverReviewable($event->takeover, $loan, $caller),
                $admin
            );
        }
    }
}
