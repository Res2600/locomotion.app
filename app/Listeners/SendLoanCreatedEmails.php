<?php

namespace App\Listeners;

use App\Events\LoanCreatedEvent;
use App\Mail\Loan\Created as LoanCreated;
use App\Models\User;
use Mail;

class SendLoanCreatedEmails
{
    /*
       Send loan-created notification to owner if loanable is not self-service
       and borrower is not also the owner.
    */
    public function handle(LoanCreatedEvent $event)
    {
        $loan = $event->loan;
        $loanable = $loan->loanable;
        $borrower = $loan->borrower;

        if (!$loanable->is_self_service) {
            $loanable->queueMailToOwners(function (User $user) use (
                $borrower,
                $loan
            ) {
                return new LoanCreated($borrower, $user, $loan);
            }, $borrower->user);
        }
    }
}
