<?php

namespace App\Listeners;

use App\Events\LoanHandoverContestedEvent;
use App\Mail\Loan\HandoverContested as LoanHandoverContested;
use App\Mail\Loan\HandoverReviewable as LoanHandoverReviewable;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanHandoverContestedEmails
{
    /*
       Send loan-handover-contested notification to:
         - owner if the borrower has contested
         - borrower if the owner has contested

       Also notify admins because they are the only ones who can resolve contestations.

       These rules apply for on-demand as well as self-service vehicles.
    */
    public function handle(LoanHandoverContestedEvent $event)
    {
        $loan = $event->handover->loan;
        $caller = $event->user;
        $borrower = $loan->borrower;

        if ($caller->id !== $borrower->user->id) {
            UserMail::queue(
                new LoanHandoverContested(
                    $event->handover,
                    $loan,
                    $borrower->user,
                    $caller
                ),
                $borrower->user
            );
        }

        $loan->loanable->queueMailToOwners(function (User $user) use (
            $event,
            $loan,
            $caller
        ) {
            return new LoanHandoverContested(
                $event->handover,
                $loan,
                $user,
                $caller
            );
        }, $caller);

        foreach ($loan->community->admins() as $admin) {
            UserMail::queue(
                new LoanHandoverReviewable($event->handover, $loan, $caller),
                $admin
            );
        }
    }
}
