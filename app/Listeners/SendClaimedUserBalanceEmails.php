<?php

namespace App\Listeners;

use App\Events\ClaimedUserBalanceEvent;
use App\Mail\UserMail;
use App\Models\User;
use App\Mail\UserClaimedBalance;

class SendClaimedUserBalanceEmails
{
    public function handle(ClaimedUserBalanceEvent $event)
    {
        $admins = User::globalAdmins()->get();

        foreach ($admins as $admin) {
            UserMail::queue(new UserClaimedBalance($event->user), $admin);
        }
    }
}
