<?php

namespace App\Listeners;

use App\Events\LoanExtensionCreatedEvent;
use App\Mail\LoanExtensionCreated;
use App\Models\User;

class SendLoanExtensionCreatedEmails
{
    /*
       Send loan-extension-created notification to borrower if loanable is
       not self service and borrower is not also the owner.
    */
    public function handle(LoanExtensionCreatedEvent $event)
    {
        $loan = $event->extension->loan;
        $loanable = $loan->loanable;
        $borrower = $loan->borrower;

        if (!$loanable->is_self_service) {
            $loanable->queueMailToOwners(function (User $user) use (
                $event,
                $loan,
                $borrower
            ) {
                return new LoanExtensionCreated(
                    $event->extension,
                    $loan,
                    $borrower,
                    $user
                );
            }, $borrower->user);
        }
    }
}
