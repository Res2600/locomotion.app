<?php

namespace App\Listeners;

use App\Events\LoanUpdatedEvent;
use App\Mail\Loan\Updated;
use App\Models\User;

class SendLoanUpdatedEmails
{
    public function __construct()
    {
    }

    public function handle(LoanUpdatedEvent $event): void
    {
        $loan = $event->loan;
        $loanable = $loan->loanable;

        if (
            !$loanable->is_self_service &&
            !$loanable->isOwnerOrCoowner($event->updater)
        ) {
            $loanable->queueMailToOwners(
                fn(User $user) => new Updated($event->updater, $user, $loan)
            );
        }
    }
}
