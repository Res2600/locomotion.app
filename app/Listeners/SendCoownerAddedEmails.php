<?php

namespace App\Listeners;

use App\Events\CoownerAddedEvent;
use App\Mail\CoownerAddedMail;
use App\Mail\UserMail;

class SendCoownerAddedEmails
{
    public function handle(CoownerAddedEvent $event)
    {
        UserMail::queue(
            new CoownerAddedMail(
                $event->adder,
                $event->coowner->user,
                $event->loanable
            ),
            $event->coowner->user
        );
    }
}
