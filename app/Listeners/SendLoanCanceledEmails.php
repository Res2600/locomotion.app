<?php

namespace App\Listeners;

use App\Events\Loan\CanceledEvent;
use App\Mail\Loan\Canceled as LoanCanceled;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanCanceledEmails
{
    /*
       For on-demand vehicles:
         Whoever cancels the loan, then the counterpart should be notified.

       For self-service vehicles
         If the loan is cancelled by the owner, then the borrower should be notified.
         If the loan is cancelled by the borrower, it is not necessary to notify the owner.
    */
    public function handle(CanceledEvent $event)
    {
        $sender = $event->user;
        $loan = $event->loan;
        $loanable = $loan->loanable;
        $borrower = $loan->borrower;

        if (!$loanable->is_self_service) {
            $loanable->queueMailToOwners(function (User $user) use (
                $sender,
                $loan
            ) {
                return new LoanCanceled($sender, $user, $loan);
            }, $sender);
        }

        if ($borrower && $borrower->user->id !== $sender->id) {
            UserMail::queue(
                new LoanCanceled($sender, $borrower->user, $loan),
                $borrower->user
            );
        }
    }
}
