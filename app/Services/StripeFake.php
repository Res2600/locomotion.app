<?php

namespace App\Services;

use App\Models\PaymentMethod;
use App\Models\User;
use Faker\Factory;

class StripeFake
{
    private \Faker\Generator $faker;

    public static function computeAmountWithFee($amount, $paymentMethod)
    {
        return StripeService::computeAmountWithFee($amount, $paymentMethod);
    }

    public function __construct()
    {
        $this->faker = Factory::create("fr_CA");
    }

    public function getSource(PaymentMethod $method)
    {
        return null;
    }

    public function getUserCustomer(User $user)
    {
        return (object) [
            "id" => $user->id,
        ];
    }

    public function createCardBySourceId($customerId, $sourceId)
    {
        return (object) [
            "id" => $sourceId,
            "external_id" => $this->faker->uuid,
            "country" => $this->faker->countryCode,
        ];
    }

    public function deleteSource($customerId, $sourceId)
    {
        return true;
    }

    public function createCharge(
        $amountWithFeeInCents,
        $customerId,
        $description,
        $paymentMethodId
    ) {
        return (object) ["id" => $this->faker->uuid];
    }
}
