<?php

namespace App\Providers;

use App\Models\Bike;
use App\Models\Car;
use App\Models\Policies\LoanablePolicy;
use App\Models\Trailer;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}
