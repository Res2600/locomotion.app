<?php

namespace App\Providers;

use App\Models\Bike;
use App\Models\Borrower;
use App\Models\Car;
use App\Models\Handover;
use App\Models\Loanable;
use App\Models\Pivots\CommunityUser;
use App\Models\Takeover;
use App\Models\Trailer;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Client;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    public function boot()
    {
        Client::creating(function (Client $client) {
            $client->incrementing = false;
            $client->id = $this->generateClientId();
        });

        Client::retrieved(function (Client $client) {
            $client->incrementing = false;
        });

        Blade::directive("money", function ($amount) {
            return "<?php echo number_format($amount, 2, ',', ' ') . '$'; ?>";
        });

        Relation::enforceMorphMap([
            "loanable" => Loanable::class,
            "car" => Car::class,
            "bike" => Bike::class,
            "trailer" => Trailer::class,

            "user" => User::class,
            "communityUser" => CommunityUser::class,
            "borrower" => Borrower::class,

            "handover" => Handover::class,
            "takeover" => Takeover::class,
        ]);
    }

    private function generateClientId(
        int $length = 16,
        string $keyspace = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    ): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }

        $pieces = [];
        $max = mb_strlen($keyspace, "8bit") - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }

        return implode("", $pieces);
    }
}
