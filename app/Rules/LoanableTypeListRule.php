<?php

namespace App\Rules;

use App\Enums\LoanableTypes;
use Illuminate\Contracts\Validation\Rule;

class LoanableTypeListRule implements Rule
{
    public function __construct()
    {
    }

    public function passes($attribute, $value): bool
    {
        $typesToValidate = explode(",", $value);
        return empty(
            array_diff($typesToValidate, LoanableTypes::possibleTypes)
        );
    }

    public function message(): string
    {
        return __("validation.custom.loanable_types", [
            "validValues" => implode(",", LoanableTypes::possibleTypes),
        ]);
    }
}
