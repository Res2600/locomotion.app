<?php

namespace App\Facades;

use App\Services\StripeFake;
use Illuminate\Support\Facades\Facade;

class Stripe extends Facade
{
    public static function fake()
    {
        static::swap($fake = new StripeFake());

        return $fake;
    }

    protected static function getFacadeAccessor()
    {
        return "stripe";
    }
}
