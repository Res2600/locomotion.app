Conseils de rédaction :

-   Titre : Précis, il doit dire ce que vous voulez obtenir, en tant qu'utilisateur, admin, visiteur unique...
-   Un ticket = un besoin (anomalie ou nouveau comportement)
-   Impact utilisateur : en quoi cette anomalie est bloquante pour vous et nos utilisateurs ?
-   Temporalité : quelle est la date cible idéale ?

Si anomalie: choisissez le template de description 'Bug'

## Résumé

(En une phrase, que voulez vous voir changer.)

## Description

(Détail des changements attendu.)

/label ~"type::amelioration"
