Please insert here the email or the ID of the customer who is requesting we delete his/her account: example@gmail.com

Steps for the team member making the request on behalf of the customer:

-   [ ] Make sure he or she is the actual owner of the email.

Steps for the tech team:

-   [ ] Execute the artisan command DeleteUser (to be created)
-   [ ] Remove from Mailchimp
-   [ ] Remove from Noke
-   ...
