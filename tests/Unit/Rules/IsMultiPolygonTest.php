<?php

namespace Tests\Unit\Rules;

use App\Rules\IsGeoJsonMultiPolygon;
use GeoJson\Geometry\MultiPolygon;
use Tests\TestCase;

class IsMultiPolygonTest extends TestCase
{
    public function testPassesEmpty()
    {
        $rule = new IsGeoJsonMultiPolygon();

        $this->assertTrue($rule->passes("polygon", null));
        $this->assertTrue($rule->passes("polygon", []));
    }

    public function testFailsOnePoint()
    {
        $rule = new IsGeoJsonMultiPolygon();

        // "Open" single-point polygon is not valid.
        $coordinates = [[[[55.234567, -33.456789]]]];

        $this->assertFalse($rule->passes("polygon", $coordinates));

        // "Closed" single-point polygon is not valid.
        $coordinates = [[[[55.234567, -33.456789], [55.234567, -33.456789]]]];

        $this->assertFalse($rule->passes("polygon", $coordinates));
    }

    public function testPassesTriangle()
    {
        $rule = new IsGeoJsonMultiPolygon();

        // "Open" triangle is not valid.
        $coordinates = [
            [
                [
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                ],
            ],
        ];

        $this->assertFalse($rule->passes("polygon", $coordinates));

        // "Closed" triangle is valid.
        $coordinates = [
            [
                [
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                    [55.234567, -33.456789],
                ],
            ],
        ];

        $this->assertTrue($rule->passes("polygon", $coordinates));
    }

    public function testPassesGeoJson()
    {
        $rule = new IsGeoJsonMultiPolygon();

        // "Closed" triangle is valid.
        $coordinates = new MultiPolygon([
            [
                [
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                    [55.234567, -33.456789],
                ],
            ],
        ]);

        $this->assertTrue($rule->passes("polygon", $coordinates));
    }

    public function testPassesStructuredObject()
    {
        $rule = new IsGeoJsonMultiPolygon();

        // "Closed" triangle is valid.
        $coordinates = [
            "type" => "MultiPolygon",
            "coordinates" => [
                [
                    [
                        [55.234567, -33.456789],
                        [56.789012, -34.56789],
                        [54.321098, -32.109876],
                        [55.234567, -33.456789],
                    ],
                ],
            ],
        ];

        $this->assertTrue($rule->passes("polygon", $coordinates));
    }

    public function testPassesMultipleTriangle()
    {
        $rule = new IsGeoJsonMultiPolygon();

        // Multiple polygons is valid.
        $coordinates = [
            [
                [
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                    [55.234567, -33.456789],
                ],
            ],
            [
                [
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                    [55.234567, -33.456789],
                ],
            ],
        ];

        $this->assertTrue($rule->passes("polygon", $coordinates));

        // Multiple lineRings is valid.
        $coordinates = [
            [
                [
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                    [55.234567, -33.456789],
                ],
                [
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                    [55.234567, -33.456789],
                ],
            ],
        ];

        $this->assertTrue($rule->passes("polygon", $coordinates));
    }

    public function testPassesInvalidTypes()
    {
        $rule = new IsGeoJsonMultiPolygon();

        // Anything not an array or null should
        // not be accepted. Test just a few
        // here.

        // Numbers not accepted.
        $this->assertFalse($rule->passes("polygon", 2.3456));
        // Strings not accepted.
        $this->assertFalse($rule->passes("polygon", "A string"));
        // arrays without coordinates are not accepted.
        $this->assertFalse(
            $rule->passes("polygon", [
                "notCoordinates" => "would fail",
            ])
        );
    }

    public function testMessage()
    {
        $rule = new IsGeoJsonMultiPolygon();

        $this->assertNotEmpty($rule->message());
    }
}
