<?php

namespace Tests\Unit\Models;

use App\Enums\LoanableTypes;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Noke;
use Stripe;
use Tests\TestCase;

class UserTest extends TestCase
{
    public $model;

    public function setUp(): void
    {
        parent::setUp();

        $this->model = new User();
    }

    public function testUpdateBalance()
    {
        $user = User::factory()->create();

        $this->assertEquals(0, $user->balance);

        $user->addToBalance(10.1);
        $this->assertEquals(10.1, $user->balance);

        $user->updateBalance(-2.8);
        $this->assertEquals(7.3, $user->balance);

        $user->updateBalance(5);
        $this->assertEquals(12.3, $user->balance);

        $user->removeFromBalance(10);
        $this->assertEquals(2.3, $user->balance);
    }

    public function testRemoveFromBalanceBelowZero()
    {
        $user = User::factory()->create([
            "balance" => 1,
        ]);

        $this->assertEquals(1, $user->balance);
        $user->removeFromBalance(1);

        $this->assertEquals(0, $user->balance);

        $user->balance = 1;
        $user->save();

        $this->assertEquals(1, $user->balance);

        // If the balance is not sufficient, abort
        $this->expectException(
            \Symfony\Component\HttpKernel\Exception\HttpException::class
        );
        $user->removeFromBalance(1.01);
        $this->assertEquals(0, 1); // Raised above
    }

    public function testUpdateUserEmailFromModelDirectly()
    {
        $user = User::factory()->create([
            "email" => "original@user.email",
        ]);

        $originalEmail = $user->email;
        $changedEmail = "changed@email.com";

        Noke::shouldReceive("findUserByEmail")
            ->withArgs(fn($a) => $a === $originalEmail)
            ->andReturns(
                (object) [
                    "username" => $originalEmail,
                ]
            )
            ->once();

        Noke::shouldReceive("updateUser")
            ->withArgs(fn($arg) => $arg->username === $changedEmail)
            ->once();

        $user->email = $changedEmail;
        $user->save();
    }

    public function testUserGetNokeUser()
    {
        $user = User::factory()->create();

        Noke::shouldReceive("findOrCreateUser")->once();

        $user->getNokeUser();
    }

    public function testUserStripeCustomerMethod()
    {
        $user = User::factory()->create();

        Stripe::shouldReceive("getUserCustomer")
            ->once()
            ->with($user);

        $user->getStripeCustomer();
    }

    public function testUpdateEmailSuccess()
    {
        $newUser = $this->createTestUser();
        $this->actingAs($newUser);

        $this->assertEquals("test@locomotion.app", $newUser->email);

        $data = [
            "email" => "test_changed@locomotion.app",
            "password" => "locomotion",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/email",
            $data
        );
        $json = $response->json();

        $response->assertStatus(200);
        $this->assertEquals(
            "test_changed@locomotion.app",
            \Illuminate\Support\Arr::get($json, "email")
        );
    }

    public function testUpdateEmailError()
    {
        $newUser = $this->createTestUser();
        $this->actingAs($newUser);

        $this->assertEquals("test@locomotion.app", $newUser->email);

        $data = [
            "email" => "test_changed@locomotion.app",
            "password" => "wrongpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/email",
            $data
        );
        $json = $response->json();

        $response->assertStatus(401);
        $this->assertEquals("test@locomotion.app", array_get($json, "email"));
    }

    public function testUpdateEmailExistingEmail()
    {
        $newUser = $this->createTestUser();

        $otherUser = User::factory()->create([
            "email" => "used@locomotion.app",
            "password" => Hash::make("locomotion"),
            "role" => null,
        ]);

        $this->actingAs($newUser);

        $this->assertEquals("test@locomotion.app", $newUser->email);
        $this->assertEquals("used@locomotion.app", $otherUser->email);

        $data = [
            "email" => "used@locomotion.app",
            "password" => "locomotion",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/email",
            $data
        );

        $response->assertStatus(422);
    }

    public function testUpdatePasswordSuccess()
    {
        $newUser = $this->createTestUser();
        $this->actingAs($newUser);

        $this->assertTrue(Hash::check("locomotion", $newUser->password));

        $data = [
            "current" => "locomotion",
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(200);
        $this->assertTrue(Hash::check("newpassword", $password));
        $this->assertFalse(Hash::check("locomotion", $password));
    }

    public function testUpdatePasswordError()
    {
        $newUser = $this->createTestUser();
        $this->actingAs($newUser);

        $this->assertTrue(Hash::check("locomotion", $newUser->password));

        $data = [
            "current" => "wrongpassword",
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(401);
        $this->assertTrue(Hash::check("locomotion", $password));
        $this->assertFalse(Hash::check("newpassword", $password));
    }

    public function testUserAvailableLoanableTypes()
    {
        $carOnlyCommunity = Community::factory()->create();
        $carOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Car->getTypeDetails());

        $bikeOnlyCommunity = Community::factory()->create();
        $bikeOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Bike->getTypeDetails());

        $user = User::factory()->create();
        $user->communities()->attach($carOnlyCommunity, [
            "approved_at" => Carbon::now(),
        ]);
        $user->communities()->attach($bikeOnlyCommunity, [
            "approved_at" => Carbon::now(),
        ]);

        $user->refresh();

        self::assertEquals(
            ["bike", "car"],
            array_sort_recursive($user->available_loanable_types)
        );
    }

    public function testUserAvailableLoanableTypes_doesntIncludeUnapprovedCommunities()
    {
        $carOnlyCommunity = Community::factory()->create();
        $carOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Car->getTypeDetails());

        $bikeOnlyCommunity = Community::factory()->create();
        $bikeOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Bike->getTypeDetails());

        $user = User::factory()->create();
        // Not approved in carCommunity
        $user->communities()->attach($carOnlyCommunity);
        $user->communities()->attach($bikeOnlyCommunity, [
            "approved_at" => Carbon::now(),
        ]);

        $user->refresh();

        self::assertEquals(["bike"], $user->available_loanable_types);
    }

    public function testUserAvailableLoanableTypes_doesntDuplicateTypes()
    {
        $carBikeCommunity = Community::factory()->create();
        $carBikeCommunity
            ->allowedLoanableTypes()
            ->sync([
                LoanableTypes::Car->getTypeDetails()->id,
                LoanableTypes::Bike->getTypeDetails()->id,
            ]);

        $carTrailerCommunity = Community::factory()->create();
        $carTrailerCommunity
            ->allowedLoanableTypes()
            ->sync([
                LoanableTypes::Car->getTypeDetails()->id,
                LoanableTypes::Trailer->getTypeDetails()->id,
            ]);

        $bikeTrailerCommunity = Community::factory()->create();
        $bikeTrailerCommunity
            ->allowedLoanableTypes()
            ->sync([
                LoanableTypes::Bike->getTypeDetails()->id,
                LoanableTypes::Trailer->getTypeDetails()->id,
            ]);

        $user = User::factory()->create();
        $user->communities()->attach($carBikeCommunity, [
            "approved_at" => Carbon::now(),
        ]);
        $user->communities()->attach($carTrailerCommunity, [
            "approved_at" => Carbon::now(),
        ]);
        $user->communities()->attach($bikeTrailerCommunity, [
            "approved_at" => Carbon::now(),
        ]);

        $user->refresh();

        self::assertEquals(
            ["bike", "car", "trailer"],
            array_sort_recursive($user->available_loanable_types)
        );
    }

    public function testUpdatePassword_AdminCanChangeOtherUsersPassword()
    {
        $newUser = User::factory()->create([
            "email" => "test@locomotion.app",
            "password" => Hash::make("locomotion"),
            "role" => "admin",
        ]);
        // Don't act as user.

        $this->assertTrue(Hash::check("locomotion", $newUser->password));

        $data = [
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(200);
        $this->assertTrue(Hash::check("newpassword", $password));
        $this->assertFalse(Hash::check("locomotion", $password));
    }

    public function testUpdatePassword_AdminMustEnterOwnPassword()
    {
        $newUser = User::factory()->create([
            "email" => "test@locomotion.app",
            "password" => Hash::make("locomotion"),
            "role" => "admin",
        ]);
        // Do act as user.
        $this->actingAs($newUser);

        $this->assertTrue(Hash::check("locomotion", $newUser->password));

        $data = [
            "current" => "wrongpassword",
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(401);
        $this->assertTrue(Hash::check("locomotion", $password));
        $this->assertFalse(Hash::check("newpassword", $password));

        $data = [
            "current" => "locomotion",
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(200);
        $this->assertTrue(Hash::check("newpassword", $password));
        $this->assertFalse(Hash::check("locomotion", $password));
    }

    private function createTestUser()
    {
        $user = User::factory()->create([
            "email" => "test@locomotion.app",
            "password" => Hash::make("locomotion"),
            "role" => null,
        ]);

        return $user;
    }

    public function testForEdit_showsAllForGlobalAdmin()
    {
        User::factory()
            ->withCommunity()
            ->create();
        User::factory()
            ->withCommunity()
            ->create();

        // 3, including $this->user
        self::assertCount(3, User::for("edit", $this->user)->get());
    }

    public function testForEdit_showsOnlyCommunityForCommunityAdmin()
    {
        $someUser = User::factory()
            ->withCommunity()
            ->create();
        User::factory()
            ->withCommunity()
            ->create();

        $communityAdmin = User::factory()
            ->hasAttached($someUser->main_community, [
                "approved_at" => new \DateTime(),
                "role" => "admin",
            ])
            ->create();

        // 2, including $communityAdmin
        self::assertCount(2, User::for("edit", $communityAdmin)->get());
    }

    public function testForEdit_showsNoneForNotAdmin()
    {
        $someUser = User::factory()
            ->withCommunity()
            ->create();
        $otherUser = User::factory()
            ->withCommunity()
            ->create();

        $regularUser = User::factory()
            ->hasAttached($someUser->main_community, [
                "approved_at" => new \DateTime(),
            ])
            ->hasAttached($otherUser->main_community, [
                "approved_at" => new \DateTime(),
            ])
            ->create();

        self::assertCount(0, User::for("edit", $regularUser)->get());
    }

    public function testDeleteUser_removesConnectionTokens()
    {
        $user = User::factory()->create();
        $user->createToken("test");
        $user->refresh();
        self::assertEquals(1, $user->tokens->count());
        self::assertFalse($user->tokens[0]->revoked);

        $user->delete();
        $user->refresh();
        self::assertTrue($user->tokens[0]->revoked);

        $this->artisan("passport:purge");
        $user->refresh();
        self::assertEmpty($user->tokens);
    }

    public function testDeleteUser_deletesLoanables()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();
        $owner = Owner::factory()->create([
            "user_id" => $user,
        ]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner,
        ]);

        self::assertFalse($loanable->trashed());

        $user->delete();
        $loanable->refresh();

        self::assertTrue($loanable->trashed());
    }

    public function testDeleteUser_cancelsLoans()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();

        $borrower = Borrower::factory()->create(["user_id" => $user]);

        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create(["borrower_id" => $borrower]);

        self::assertEquals("in_process", $loan->status);

        $user->delete();
        $loan->refresh();

        self::assertEquals("canceled", $loan->status);
    }

    public function testDeleteUsers_removesPrivileges()
    {
        $user = User::factory()
            ->adminOfCommunity()
            ->create([
                "role" => "admin",
            ]);

        self::assertEquals("admin", $user->role);
        self::assertEquals("admin", $user->communities[0]->pivot->role);

        $user->delete();
        $user->refresh();

        self::assertNull($user->role);
        self::assertNull($user->communities[0]->pivot->role);
    }

    public function testDeleteUsers_suspendsFormActiveCommunities()
    {
        $approvedCommunity = Community::factory()->create([
            "name" => "approved_community",
        ]);
        $unapprovedCommunity = Community::factory()->create([
            "name" => "unapproved_community",
        ]);
        $suspendedCommunity = Community::factory()->create([
            "name" => "suspended_community",
        ]);
        $suspensionTime = Carbon::now()->subHour();
        $user = User::factory()
            ->withCommunity($approvedCommunity)
            ->hasAttached($unapprovedCommunity)
            ->hasAttached($suspendedCommunity, [
                "approved_at" => Carbon::now()->subDay(),
                "suspended_at" => $suspensionTime,
            ])
            ->create();

        $user->delete();
        $user->refresh();

        self::assertCount(3, $user->communities);

        $userApprovedCommunity = $user->communities->first(
            fn($c) => $c->name === "approved_community"
        );
        self::assertNotNull($userApprovedCommunity->pivot->suspended_at);

        $userUnapprovedCommunity = $user->communities->first(
            fn($c) => $c->name === "unapproved_community"
        );
        self::assertNull($userUnapprovedCommunity->pivot->suspended_at);
        self::assertNull($userUnapprovedCommunity->pivot->suspended_at);

        $userSuspendedCommunity = $user->communities->first(
            fn($c) => $c->name === "suspended_community"
        );
        self::assertEquals(
            $suspensionTime->format("Y-m-d H:i:s"),
            (new Carbon($userSuspendedCommunity->pivot->suspended_at))->format(
                "Y-m-d H:i:s"
            )
        );
    }
}
