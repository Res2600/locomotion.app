<?php

namespace Tests\Unit\Listeners;

use App\Events\BorrowerCompletedEvent;
use App\Listeners\SendBorrowerCompletedEmails;
use App\Mail\Borrower\Completed as BorrowerCompleted;
use App\Mail\Borrower\Reviewable as BorrowerReviewable;
use App\Models\Community;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class SendBorrowerCompletedEmailsTest extends TestCase
{
    /*
  This tests that the email is indeed sent to the borrower.
  It also indirectly tests the case of an app instance without admin.
*/
    public function testSendsEmailToBorrower()
    {
        Mail::fake();

        $user = User::factory()->create();
        $community = Community::factory()->create();
        $user
            ->communities()
            ->attach($community->id, ["approved_at" => new \DateTime()]);

        $event = new BorrowerCompletedEvent($user);

        // Don't trigger event. Only test listener.
        $listener = app()->make(SendBorrowerCompletedEmails::class);
        $listener->handle($event);

        // Mail to borrower.
        Mail::assertQueued(BorrowerCompleted::class, function ($mail) use (
            $user
        ) {
            return $mail->hasTo($user->email);
        });
    }

    public function testSendsEmailToGlobalAdmins()
    {
        Mail::fake();
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $global_admin = User::factory()->create(["role" => "admin"]);
        $community_admin = User::factory()->create();
        $community->users()->attach($community_admin, ["role" => "admin"]);
        $community->users()->attach($user);

        $event = new BorrowerCompletedEvent($user);

        // Don't trigger event. Only test listener.
        $listener = app()->make(SendBorrowerCompletedEmails::class);
        $listener->handle($event);

        // Mail to global admin
        Mail::assertQueued(BorrowerReviewable::class, function ($mail) use (
            $global_admin
        ) {
            return $mail->hasTo($global_admin->email);
        });

        // Check that community admin will not get email.
        Mail::assertNotQueued(BorrowerReviewable::class, function ($mail) use (
            $community_admin
        ) {
            return $mail->hasTo($community_admin->email);
        });
    }
}
