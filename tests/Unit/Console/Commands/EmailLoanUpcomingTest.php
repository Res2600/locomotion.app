<?php

namespace Tests\Unit\Console\Commands;

use App\Mail\Loan\Upcoming;
use App\Mail\Loan\UpcomingAsOwner;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Carbon\Carbon;
use Log;

use Mail;
use Tests\TestCase;

class EmailLoanUpcomingTest extends TestCase
{
    public function testUpcomingLoanNotSelfService()
    {
        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()
            ->withCoowner()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => false,
            ]);

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);
        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertQueued(
            Upcoming::class,
            fn(Upcoming $upcoming) => $upcoming->user->is($borrowerUser)
        );
        Mail::assertQueued(
            UpcomingAsOwner::class,
            fn(UpcomingAsOwner $upcoming) => $upcoming->user->is($ownerUser)
        );
        Mail::assertQueued(
            UpcomingAsOwner::class,
            fn(UpcomingAsOwner $upcoming) => $upcoming->user->is(
                $bike->coowners[0]->user
            )
        );

        // Reload from database.
        $loan->refresh();
        // Check that the email is marked as sent.
        $this->assertEquals(["sent_loan_upcoming_email" => true], $loan->meta);
    }

    public function testUpcomingLoanNotSelfService_canceled()
    {
        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()
            ->withCoowner()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => false,
            ]);

        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);
        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertNothingQueued();

        // Reload from database.
        $loan->refresh();
        // Check that the email is marked as sent.
        $this->assertEquals([], $loan->meta);
    }

    public function testUpcomingLoanSelfService()
    {
        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()->create([
            "owner_id" => $ownerUser->owner->id,
            "is_self_service" => true,
        ]);

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);

        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertQueued(
            Upcoming::class,
            fn(Upcoming $upcoming) => $upcoming->user->is($borrowerUser)
        );
        Mail::assertNotQueued(UpcomingAsOwner::class);

        // Reload from database.
        $loan->refresh();
        // Check that the email is marked as sent.
        $this->assertEquals(["sent_loan_upcoming_email" => true], $loan->meta);
    }

    public function testLoanNotYetUpcomingSelfService()
    {
        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()->create([
            "owner_id" => $ownerUser->owner->id,
            "is_self_service" => true,
        ]);

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in more than 3 hours.
                "departure_at" => Carbon::now()->add(185, "minutes"),
                "duration_in_minutes" => 600,
            ]);
        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertNothingQueued();
        // Reload from database.
        $loan->refresh();
        // Check that the email is not marked as sent.
        $this->assertEquals([], $loan->meta);
    }

    public function testLoanAlreadyDepartedSelfService()
    {
        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()->create([
            "owner_id" => $ownerUser->owner->id,
            "is_self_service" => true,
        ]);

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan departed already.
                "departure_at" => Carbon::now()->subtract(5, "minutes"),
                "duration_in_minutes" => 600,
            ]);

        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertNothingQueued();
        // Reload from database.
        $loan->refresh();
        // Check that the email is not marked as sent.
        $this->assertEquals([], $loan->meta);
    }

    public function testLoanCreatedLessThanThreeHoursAgoSelfService()
    {
        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()->create([
            "owner_id" => $ownerUser->owner->id,
            "is_self_service" => true,
        ]);

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created less than 3 hours ago.
                "created_at" => Carbon::now()->subtract(175, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);
        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertNothingQueued();
        // Reload from database.
        $loan->refresh();
        // Check that the email is not marked as sent.
        $this->assertEquals([], $loan->meta);
    }

    public function testUpcomingLoanNotSelfServiceWithPretendOption()
    {
        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()->create([
            "owner_id" => $ownerUser->owner->id,
            "is_self_service" => false,
        ]);

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);

        Mail::fake();
        Log::spy();

        $this->artisan("email:loan:upcoming", [
            "--pretend" => true,
        ])->assertExitCode(0);

        // Example of the expected calls to Log::info()
        //   Fetching loans starting in three hours or less created at least three hours before now...
        //   Would have sent LoanUpcoming email to borrower at: frederic.richard@example.org for loan with id: 1
        //   Would have sent LoanUpcoming email to owner at: frederic.richard@example.org for loan with id: 1
        //   Done.
        Log::shouldHaveReceived("info")->times(4);
        Mail::assertNothingQueued();

        // Reload from database.
        $loan->refresh();
        // Check that the email is not marked as sent.
        $this->assertEquals([], $loan->meta);
    }
}
