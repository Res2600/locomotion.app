<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\NokeSyncUsers as NokeSyncUsersCommand;
use App\Models\Community;
use App\Models\User;
use App\Services\NokeService;
use Tests\TestCase;

class NokeSyncUsersTest extends TestCase
{
    public function testNokeSyncUsersCommand_getLocalUsers()
    {
        $communityUsingNoke = Community::factory()->create([
            "uses_noke" => true,
        ]);
        $communityNotUsingNoke = Community::factory()->create([
            "uses_noke" => false,
        ]);

        $userWithoutCommunity = User::factory()
            ->withBorrower()
            ->create([]);

        $userNotApprovedWithNoke = User::factory()
            ->withBorrower()
            ->hasAttached($communityUsingNoke, [])
            ->create([]);
        $userNotApprovedWithoutNoke = User::factory()
            ->withBorrower()
            ->hasAttached($communityNotUsingNoke, [])
            ->create([]);

        $userApprovedWithNoke = User::factory()
            ->hasAttached($communityUsingNoke, [
                "approved_at" => new \DateTime(),
            ])
            ->withBorrower()
            ->create([]);
        $userApprovedWithoutNoke = User::factory()
            ->withBorrower()
            ->hasAttached($communityNotUsingNoke, [
                "approved_at" => new \DateTime(),
            ])
            ->create([]);

        $users = NokeSyncUsersCommand::getLocalUsers();

        $this->assertEquals(1, $users->count());

        $this->assertTrue(
            $users->contains(function ($user, $key) use (
                $userApprovedWithNoke
            ) {
                return $user->id == $userApprovedWithNoke->id;
            })
        );
    }
}
