<?php

namespace Tests\Integration;

use Carbon\Carbon;
use function PHPUnit\Framework\assertEquals;

class PaymentTest extends ActionTestCase
{
    public function testCompletePayments()
    {
        // Reset test time now.
        Carbon::setTestNow();

        $loan = $this->buildLoan("payment");
        $loan->borrower->user->balance = 20;
        $loan->borrower->user->save();
        $loan->borrower_validated_at = Carbon::now();
        $loan->owner_validated_at = Carbon::now();
        $loan->platform_tip = 0;
        $loan->save();

        $pricing = $loan->community->pricings[0];
        $pricing->rule = "5";
        $pricing->save();

        $executedAt = Carbon::now()->addMinutes(15);
        Carbon::setTestNow($executedAt);

        $payment = $loan->payment;
        $this->assertNotNull($payment);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/payment/complete",
            [
                "type" => "payment",
            ]
        );
        $response->assertStatus(200);

        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id/actions/$payment->id"
        );

        $payment->refresh();
        assertEquals("completed", $payment->status);
        assertEquals(
            $executedAt->toDateTimeString(),
            (new Carbon($payment->executed_at))->toDateTimeString()
        );

        $loan->refresh();
        assertEquals(15, $loan->borrower->user->balance);

        $this->json("PUT", "/api/v1/loans/$loan->id/payment/complete", [
            "type" => "payment",
        ])->assertStatus(403);
    }

    public function testCompletePayments_failsIfBeforeLoanDeparture()
    {
        // Reset test time now.
        Carbon::setTestNow();

        $loan = $this->buildLoan("payment");

        $executedAt = Carbon::now()->subMinutes(15);
        Carbon::setTestNow($executedAt);

        $payment = $loan->payment;
        $this->assertNotNull($payment);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/payment/complete",
            [
                "type" => "payment",
            ]
        );
        $response->assertStatus(403);

        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id/actions/$payment->id"
        );

        $payment->refresh();
        $this->assertEquals("in_process", $payment->status);
        $this->assertNull($payment->executed_at);
    }

    public function testCompletePayments_failsIfNotEnoughMoney()
    {
        $this->withoutEvents();

        $loan = $this->buildLoan("payment");
        $loan->platform_tip = 5;
        $loan->save();

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/payment/complete",
            [
                "type" => "payment",
            ]
        );

        $response->assertStatus(403)->assertJson([
            "message" =>
                "L'emprunteur-se n'a pas assez de fonds dans son solde pour payer présentement.",
        ]);
    }

    public function testCompletePayments_failsIfNotValidated()
    {
        $this->withoutEvents();

        $loan = $this->buildLoan("payment");
        $loan->borrower->user->balance = 20;
        $loan->borrower->user->save();

        $pricing = $loan->community->pricings[0];
        $pricing->rule = "5";
        $pricing->save();

        $this->actAs($loan->loanable->owner->user);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/payment/complete",
            [
                "type" => "payment",
                "loan_id" => $loan->id,
            ]
        );
        $response->assertStatus(403)->assertJson([
            "message" => "L'emprunt doit être validé avant d'être payé.",
        ]);
    }

    public function testCompletePayments_failsIfHandoverContested()
    {
        $this->withoutEvents();

        $loan = $this->buildLoan("payment");
        $loan->borrower->user->balance = 20;
        $loan->borrower->user->save();
        $loan->borrower_validated_at = Carbon::now();
        $loan->owner_validated_at = Carbon::now();
        $loan->handover->contest();
        $loan->handover->save();

        $pricing = $loan->community->pricings[0];
        $pricing->rule = "5";
        $pricing->save();

        $this->actAs($loan->loanable->owner->user);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/payment/complete",
            [
                "type" => "payment",
                "loan_id" => $loan->id,
            ]
        );
        $response->assertStatus(403)->assertJson([
            "message" => "L'étape handover doit être complétée.",
        ]);
    }

    public function testCompletePayments_failsIfTakeoverContested()
    {
        $this->withoutEvents();

        $loan = $this->buildLoan("payment");
        $loan->borrower->user->balance = 20;
        $loan->borrower->user->save();
        $loan->borrower_validated_at = Carbon::now();
        $loan->owner_validated_at = Carbon::now();
        $loan->takeover->contest();
        $loan->takeover->save();

        $pricing = $loan->community->pricings[0];
        $pricing->rule = "5";
        $pricing->save();

        $this->actAs($loan->loanable->owner->user);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/payment/complete",
            [
                "type" => "payment",
                "loan_id" => $loan->id,
            ]
        );
        $response->assertStatus(403)->assertJson([
            "message" => "L'étape takeover doit être complétée.",
        ]);
    }
}
