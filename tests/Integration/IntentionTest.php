<?php

namespace Tests\Integration;

use Carbon\Carbon;
use function PHPUnit\Framework\assertEquals;

class IntentionTest extends ActionTestCase
{
    public function testCompleteIntentions()
    {
        $loan = $this->buildLoan("intention");

        $intention = $loan->intention;

        $this->assertNotNull($intention);

        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        Carbon::setTestNow($executedAtDate);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/intention/complete",
            [
                "type" => "intention",
            ]
        );
        $response->assertStatus(200);

        $intention->refresh();
        self::assertEquals("completed", $intention->status);
        self::assertEquals($executedAtDate, $intention->executed_at);
    }

    public function testCancelIntentions()
    {
        $loan = $this->buildLoan("intention");

        $intention = $loan->intention;

        $this->assertNotNull($intention);

        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        Carbon::setTestNow($executedAtDate);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/intention/cancel",
            [
                "type" => "intention",
            ]
        );
        $response->assertStatus(200);

        $intention->refresh();
        assertEquals("canceled", $intention->status);
        assertEquals($executedAtDate, $intention->executed_at);
    }
}
