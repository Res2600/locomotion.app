<?php

namespace Tests\Integration;

use App\Models\Borrower;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Pricing;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

abstract class ActionTestCase extends TestCase
{
    protected function buildLoan($upTo = null)
    {
        $community = Community::factory()->create();
        Pricing::factory()->create([
            "rule" => "0",
            "community_id" => $community->id,
            "object_type" => "App\Models\Car",
        ]);

        $borrowerUser = User::factory()->create();
        $borrowerUser
            ->communities()
            ->attach($community->id, ["approved_at" => Carbon::now()]);
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser,
            "approved_at" => Carbon::now(),
        ]);

        $user = User::factory()->create();
        $user
            ->communities()
            ->attach($community->id, ["approved_at" => Carbon::now()]);

        $owner = Owner::factory()->create(["user_id" => $user->id]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "always",
            ]);

        $loanFactory = Loan::factory([
            "departure_at" => Carbon::now()->toDateTimeString(),
            // Don't generate 0-minute loans.
            "duration_in_minutes" => 15 + $this->faker->randomNumber(4),
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_id" => $borrower->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "message_for_owner" => "",
            "reason" => "Test",
            "community_id" => $community->id,
        ]);

        // Load newly created loan.
        if ($upTo === "intention") {
            return $loanFactory->withInProcessIntention()->create();
        }

        if ($upTo === "pre_payment") {
            return $loanFactory->withInProcessPrePayment()->create();
        }

        if ($upTo === "takeover") {
            return $loanFactory->withInProcessTakeover()->create();
        }

        if ($upTo === "handover") {
            return $loanFactory->withInProcessHandover()->create();
        }

        return $loanFactory->withInProcessPayment()->create();
    }
}
