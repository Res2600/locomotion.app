<?php

namespace Tests\Integration;

use Carbon\Carbon;
use App\Models\Borrower;
use App\Models\Car;
use App\Models\Community;
use App\Models\Handover;
use App\Models\Loan;
use App\Models\Owner;
use App\Models\Pricing;
use App\Models\Takeover;
use App\Models\User;
use Tests\TestCase;
use function PHPUnit\Framework\assertEquals;

class HandoverTest extends ActionTestCase
{
    public function testCreateHandoversWithActionsFlow()
    {
        $loan = $this->buildLoan("handover");

        $handover = $loan->handover;
        $this->assertNotNull($handover);
        $this->assertEquals("in_process", $handover->status);
    }

    public function testCompleteHandovers()
    {
        // Reset test time now.
        Carbon::setTestNow();

        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        Carbon::setTestNow($executedAtDate);

        $loan = $this->buildLoan("handover");

        $handover = $loan->handover;

        $this->assertNotNull($handover);
        $this->actAs($loan->borrower->user);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/handover/complete",
            [
                "type" => "handover",
                "mileage_end" => 0,
            ]
        );
        $response->assertStatus(200);
        $loan = $loan->fresh();
        $handover = $loan->handover;

        assertEquals("completed", $handover->status);
        assertEquals($executedAtDate, $handover->executed_at);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/handover/complete",
            [
                "type" => "handover",
                "mileage_end" => 0,
            ]
        );
        $response->assertStatus(403);
    }

    public function testCompleteHandovers_failsIfBeforeLoanDeparture()
    {
        // Reset test time now.
        Carbon::setTestNow();

        $loan = $this->buildLoan("handover");

        $executedAt = Carbon::now()->subMinutes(15);
        Carbon::setTestNow($executedAt);

        $handover = $loan->handover;
        $this->assertNotNull($handover);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/handover/complete",
            [
                "type" => "handover",
                "mileage_end" => 0,
            ]
        );
        $response->assertStatus(403);

        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id/actions/$handover->id"
        );

        $handover->refresh();
        $this->assertEquals("in_process", $handover->status);
        $this->assertNull($handover->executed_at);
    }

    public function testContestHandover()
    {
        // Reset test time now.
        Carbon::setTestNow();

        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        Carbon::setTestNow($executedAtDate);

        $loan = $this->buildLoan("handover");

        $handover = $loan->handover;
        $handover->complete();
        $handover->save();

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/handover/contest",
            [
                "type" => "handover",
            ]
        );
        $response->assertStatus(200);

        $handover->refresh();

        assertEquals("canceled", $handover->status);
        assertEquals($executedAtDate, $handover->executed_at);
    }
}
